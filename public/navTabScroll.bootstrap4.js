var hidWidth;
var scrollBarWidths = 40;

var scroller_left = document.getElementById("scroller-left");
var scroller_right = document.getElementById("scroller-right");
var list = document.getElementById("myTabAppOpen");
var wrapper = document.getElementById("wrapper");

var widthOfList = function(){
  var itemsWidth = 0;
  var list_a = list.querySelectorAll("a");
  for(var i = 0; i < list_a.length; i++){
    var itemWidth = this.offsetWidth;  
    itemsWidth+=itemWidth;
  }
  /*
  $('.list a').each(function(){
    var itemWidth = $(this).outerWidth();
    itemsWidth+=itemWidth;
  });
  */
  return itemsWidth;
};
var getOffsetLeft = function(elem){
  var tmp=elem;
  var left=tmp.offsetLeft;
  var top=tmp.offsetTop;
  while (tmp=tmp.offsetParent) left += tmp.offsetLeft;
  tmp=elem;
  while(tmp=tmp.offsetParent) top+=tmp.offsetTop;
  return [left,top];
}

var widthOfHidden = function(){
  
  var ww = 0 - wrapper.offsetWidth;
  var hw = ((wrapper.offsetWidth)-widthOfList()-getLeftPosi())-scrollBarWidths;
  var nav_items = document.querySelectorAll('.nav-item');
  var last_nav_item = nav_items[nav_items.length-1];
  var offset_nav_item = getOffsetLeft(last_nav_item);
  var rp = document.width - (offset_nav_item[0]+last_nav_item.offsetWidth);
  //var rp = document.width - ($('.nav-item').last().offset().left + $('.nav-item').last().outerWidth());

  /*
  var ww = 0 - $('.wrapper').outerWidth();
  var hw = (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
  //var rp = $(document).width() - ($('.nav-item.nav-link').last().offset().left + $('.nav-item.nav-link').last().outerWidth());
  var rp = $(document).width() - ($('.nav-item').last().offset().left + $('.nav-item').last().outerWidth());
  */
  if (ww>hw) {
    //return ww;
    return (rp>ww?rp:ww);
  }
  else {
    //return hw;
    return (rp>hw?rp:hw);
  }
};

var getLeftPosi = function(){
  var ww = 0 - wrapper.offsetWidth;
  var lp = list.offsetLeft;
  /*
  var ww = 0 - $('.wrapper').outerWidth();
  var lp = $('.list').position().left;
  */
  if (ww>lp) {
    return ww;
  }
  else {
    return lp;
  }
};

var reAdjust = function(){

  // check right pos of last nav item
  var nav_items = document.querySelectorAll('.nav-item');
  var last_nav_item = nav_items[nav_items.length-1];
  var offset_nav_item = getOffsetLeft(last_nav_item);
  
  var rp = document.width - (offset_nav_item[0] + last_nav_item.offsetWidth);
  if ((wrapper.offsetWidth) < widthOfList() && (rp<0)) {
    scroller_right.style.display = "flex";
  }
  else {
    scroller_right.style.display = "none";
  }

  var lef_posi = getLeftPosi();
  if(isNaN(lef_posi)) lef_posi=0;

  if (lef_posi<0) {
    scroller_left.style.display = "flex";
  }
  else {
    //list.animate({left:"-="+getLeftPosi()+"px"},'slow');  
    
    console.log('lef_posi: '+lef_posi);
    //list.animate({left:"-="+lef_posi+"px"},'slow');
    /*
    list.animate(
      {transform: 'translateX(-'+lef_posi+'px)'},
      {duration: 1000}
    )
    */
    scroller_left.style.display = "none";
  }

  //var rp = $(document).width() - ($('.nav-item.nav-link').last().offset().left + $('.nav-item.nav-link').last().outerWidth());
  /*
  var rp = $(document).width() - ($('.nav-item').last().offset().left + $('.nav-item').last().outerWidth());
  if (($('.wrapper').outerWidth()) < widthOfList() && (rp<0)) {
    $('.scroller-right').show().css('display', 'flex');
  }
  else {
    $('.scroller-right').hide();
  }

  if (getLeftPosi()<0) {
    $('.scroller-left').show().css('display', 'flex');
  }
  else {
    $('.item').animate({left:"-="+getLeftPosi()+"px"},'slow');
    $('.scroller-left').hide();
  }
  */
}

reAdjust();
window.addEventListener('resize', function(e){  
  reAdjust();
});
/*
$(window).on('resize',function(e){  
  reAdjust();
});
*/

scroller_right.addEventListener("click", function(e) {

  scroller_left.style.display = "flex";
  scroller_right.style.display = "none";
  /*
  list.animate({left:"+="+widthOfHidden()+"px"},'slow',function(){
    reAdjust();
  });
  */
});

scroller_left.addEventListener("click", function(e) {

  scroller_left.style.display = "none";
  scroller_right.style.display = "flex";

  /*
  list.animate({left:"+="+getLeftPosi()+"px"},'slow',function(){
    reAdjust();
  });
  */
});
/*
$('.scroller-right').click(function() {
  $('.scroller-left').fadeIn('slow');
  $('.scroller-right').fadeOut('slow');

  $('.list').animate({left:"+="+widthOfHidden()+"px"},'slow',function(){
    reAdjust();
  });
});

$('.scroller-left').click(function() {

  $('.scroller-right').fadeIn('slow');
  $('.scroller-left').fadeOut('slow');

  $('.list').animate({left:"-="+getLeftPosi()+"px"},'slow',function(){
    reAdjust();
  });
});    
*/