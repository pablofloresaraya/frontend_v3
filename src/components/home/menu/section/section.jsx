import React from 'react';

export function Section(props){  
    //console.log('Section props: ',props);  
    return(
        <div className="sb-sidenav-menu-heading">{props.sectionName}</div>
    );
    
}

export default Section;