import React, {Component} from 'react';
import { connect } from 'react-redux';

import { userActions } from '../../_actions';
import { config } from '../../_constants';

class Header extends Component{
  constructor(props){
    super(props);
    this.styles_navbar = {
      height: '40px', 
      paddingTop: '1px', 
      paddingBottom: '1px',
    };
    
    let user = JSON.parse(localStorage.getItem('user'));
    let company_id = user.company_used? user.company_used:-1;

    this.state = {
        company_used: company_id
    };

    this._handleClickCloseSession = this._handleClickCloseSession.bind(this);
    this._handleChangeCompany = this._handleChangeCompany.bind(this);
  }

  _handleClick(e){
    document.body.classList.toggle("sb-sidenav-toggled");
  }

  _handleClickCloseSession(e) {
      e.preventDefault();
      this.props.closeSession();
  }

  _handleChangeCompany(e){
    let user = JSON.parse(localStorage.getItem('user'));
    user.company_used = e.target.value;
    localStorage.setItem('user', JSON.stringify(user));
    this.setState({company_used: e.target.value})

    this.props.onChangeCompanyUsed(e.target.value);
  }

  /*
  <a className="dropdown-item" href="index.html">Mi Perfil</a>
  <a className="dropdown-item" href="index.html">Log de Actividades</a>
  <div className="dropdown-divider"></div>
  */
  render (){
    console.log("Header",this.props);
    
    return (
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark" style={this.styles_navbar}>
            <a className="navbar-brand" href="index.html">{config.nombreEmpresa}</a>
            <button 
              className="btn btn-link btn-sm order-1 order-lg-0" 
              id="sidebarToggle" 
              href="/#" 
              onClick={this._handleClick}>
                <i className="fas fa-bars"></i>
            </button>
            <form className="d-none d-md-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
              <select className="form-control" value={this.state.company_used} onChange={this._handleChangeCompany} >
                {this.props.user.company_list.map(row =>
                  <option key={row.company_id} value={row.company_id}>{row.identifier+'-'+row.identifier_dv+' '+row.name }</option>
                )};
              </select>
            </form>
            <form className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div className="input-group">
                </div>
            </form>
            <ul className="navbar-nav ml-auto ml-md-0">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" id="userDropdown" href="index.html" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i className="fas fa-user fa-fw"></i> {this.props.user.name}
                    </a>
                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        
                        <a className="dropdown-item" 
                            onClick={this._handleClickCloseSession}
                            href="/#"
                        >Cerrar Sesi&oacute;n
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
    );
  }
}

function mapState(state) {
    const authentication = state.authentication;
    return authentication;
}

const actionCreators = {
  closeSession: userActions.closeSession,
  useCompany: userActions.useCompany
};

const connectedHeaderSection = connect(mapState, actionCreators)(Header);
export { connectedHeaderSection as Header };