import React, {Component} from 'react';
import {Header} from './header/header'
import {Menu} from './menu/menu'
import {Main} from './main/main'
import {Footer} from './footer/footer'

export class HomeView extends Component{
    constructor(props){
      super(props);
      this.styles_layoutSidenav_content = {
        top: '41px',
      }
    }

    render(){
      return (
        <div>
          <Header showModule={this.props.menuShowModule} authentication={this.props.authentication} onChangeCompanyUsed={this.props._handleOnChangeCompanyUsed} />
          <div id="layoutSidenav">
            <Menu
                showModule={this.props.menuShowModule}
                menuModuleActive={this.props.menuModuleActive}
                menuNameModuleActive={this.props.menuNameModuleActive}
                _handleOnClickModule={this.props._handleOnClickModule}
            />
            <div id="layoutSidenav_content" style={this.styles_layoutSidenav_content}>
              <Main />
              <Footer />
            </div>
          </div>
          <div id="section_modal"></div>
        </div>
      );
    }
  }