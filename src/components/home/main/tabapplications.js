import React, {Component} from 'react';
import $ from 'jquery';

export class TabApplications extends Component{
    constructor(props){
      super(props);
      this.scrollBarWidths = 40;
      this.styles_tabopen = {
        minWidth: '3000px',
      }

      this.state = {
        scrollerRightDisplay: {display: 'none'},
        scrollerLeftDisplay: {display: 'none'},
      }

      this._widthOfHidden = this._widthOfHidden.bind(this)
      this._reAdjust = this._reAdjust.bind(this)
      this._scrollerRightClick = this._scrollerRightClick.bind(this)
      this._scrollerLeftClick = this._scrollerLeftClick.bind(this)
    }

    _widthOfList(){
      var itemsWidth = 0;
      $('.list a').each(function(){
        var itemWidth = $(this).outerWidth();
        itemsWidth+=itemWidth;
      });
      return itemsWidth;
    };

    _widthOfHidden(){
      var ww = 0 - $('.wrapper').outerWidth();
      var hw = (($('.wrapper').outerWidth())-this._widthOfList()-this._getLeftPosi())-this.scrollBarWidths;
      var rp = $(document).width() - ($('.nav-item').last().offset().left + $('.nav-item').last().outerWidth());

      if (ww>hw) {
        //return ww;
        return (rp>ww?rp:ww);
      }
      else {
        //return hw;
        return (rp>hw?rp:hw);
      }
    };

    _getLeftPosi(){
      var ww = 0 - $('.wrapper').outerWidth();
      var lp = $('.list').position().left;
      
      if (ww>lp) {
        return ww;
      }
      else {
        return lp;
      }
    };

    _reAdjust(){
      var rp = $(document).width() - ($('.nav-item').last().offset().left + $('.nav-item').last().outerWidth());

      console.log('wrapper.outerWidth'+$('.wrapper').outerWidth()+' _widthOfList: '+this._widthOfList()+' _getLeftPosi: '+this._getLeftPosi()+' rp: '+rp);

      if (($('.wrapper').outerWidth()) < this._widthOfList() && (rp<0)) {
        this.setState(
          (state,props) => ({
            scrollerRightDisplay: {display: 'flex'}
          }));
      }
      else {
        this.setState(
          (state,props) => ({
            scrollerRightDisplay: {display: 'none'}
          }));
      }
      if (this._getLeftPosi()<0) {
        this.setState(
          (state,props) => ({
            scrollerLeftDisplay: {display: 'flex'}
          }));
      }
      else {
        this.setState(
          (state,props) => ({
            scrollerLeftDisplay: {display: 'none'}
          }));
      }
    }

    _scrollerRightClick(){
      $('.scroller-left').fadeIn('slow');
      $('.scroller-right').fadeOut('slow');
    
      $('.list').animate({left:"+="+this._widthOfHidden()+"px"},'slow',function(){
        //this._reAdjust();
      });
    }

    _scrollerLeftClick(){
      $('.scroller-right').fadeIn('slow');
      $('.scroller-left').fadeOut('slow');
    
      $('.list').animate({left:"-="+this._getLeftPosi()+"px"},'slow',function(){
        //this._reAdjust();
      });
    }

    componentDidMount() {
      this._reAdjust();
      window.addEventListener('resize', this._reAdjust);
      $('.scroller-right').click(this._scrollerRightClick);
      $('.scroller-left').click(this._scrollerLeftClick);
    }
  
    render(){
      return(
        <div className="w-100">
          <div id="scroller-left" className="scroller scroller-left float-left" style={this.state.scrollerLeftDisplay}><i className="fa fa-chevron-left"></i></div>
          <div id="scroller-right" className="scroller scroller-right float-right" style={this.state.scrollerRightDisplay}><i className="fa fa-chevron-right"></i></div>
          <div id="wrapper" className="wrapper">
              <ul className="nav nav-tabs list ml-1 mt-1" id="myTabAppOpen" role="tablist" style={this.styles_tabopen}>
                  <li className="nav-item">
                      <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Inicio</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Aplicacion 1</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 2</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 3</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 4</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 5</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Aplicacion 6</a>
                  </li>
              </ul>
          </div>
        </div>
      );
    }
  }