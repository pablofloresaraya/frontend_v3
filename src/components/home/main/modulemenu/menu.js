import React, {Component} from 'react';
import { menuService } from '../../../_services';
import $ from 'jquery';

function DropdownMenu(props){
  return(
      <div className="dropdown-menu" aria-labelledby="navbarDropdown">{ props.children }</div>
  )
}

function NavItem(props){
  return(
      <li className="nav-item dropdown">{ props.children }</li>
  )
}

function NavLink(props){
  return(
    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      {props.name}
    </a>
  )
}

function DropdownItem(props){

  return(
      <a className={"dropdown-item "+props.class} href="#">{props.name}</a>
  )

}

function DropdownSubMenu(props){
  //<a class="dropdown-item dropdown-toggle" href="#">{props.name}</a>
  return (
    <div className="dropdown-submenu">
      <a className="dropdown-item dropdown-toggle" href="#">{props.name}</a>
      {props.children}
    </div>
  )
}

function NavbarNav(props){

    return(
      <ul className="navbar-nav ml-auto">
        {props.children}
      </ul>
    );
}

export class Menu extends Component{
    constructor(props) {
        super(props);
        this.state = {apps: []};
        
        this._handleOnClickApplication = this._handleOnClickApplication.bind(this);
    }

    _handleOnClickApplication(app){
        this.props.addApplicationTab(app);
    }

    componentDidMount(){

      $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        if (!$(this).next().hasClass('show')) {
          $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');
      
      
        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
          $('.dropdown-submenu .show').removeClass("show");
        });
      
      
        return false;
      });
    }

    render(){
      console.log("4. Main Menu props", this.props);

      return(
        <nav  className="navbar navbar-expand-lg navbar-light bg-light"
              style={{ paddingTop: '1px', paddingBottom: '1px', paddingRight: '10rem'}}
        >
          <a className="navbar-brand" href="#"
              style={{ color: 'rgba(162, 157, 157, 0.5)', fontSize: '1.5rem', padding: '0px'}}
          >
              <strong>{this.props.moduleName}</strong>
          </a>
          <button
                  className="navbar-toggler navbar-toggler-right"
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <NavbarNav>
              <NavItem>
                <NavLink name="Configuración" />
                <DropdownMenu>
                  <DropdownItem name="Form1" />
                  <DropdownSubMenu name="SubMenu">
                    <DropdownMenu>
                      <DropdownItem name="Form4" />
                      <DropdownItem name="Form5" />
                      <DropdownItem name="Form6" />
                    </DropdownMenu>
                  </DropdownSubMenu>
                  <DropdownItem name="Form2" />
                  <DropdownItem name="Form3" />
                </DropdownMenu>
              </NavItem>
              <NavItem>
                <NavLink name="Procesos" />
                <DropdownMenu>
                  <DropdownItem name="Form1" />
                  <DropdownItem name="Form2" />
                  <DropdownItem name="Form3" />
                </DropdownMenu>
              </NavItem>
              <NavItem>
                <NavLink name="Consultas" />
                <DropdownMenu>
                  <DropdownItem name="Form1" />
                  <DropdownItem name="Form2" />
                  <DropdownItem name="Form3" />
                </DropdownMenu>
              </NavItem>
            </NavbarNav>
          </div>
      </nav>
      );
    }
  }