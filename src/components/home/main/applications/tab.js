import React, {Component} from 'react';

export class Tab extends Component{
    constructor(props){
      super(props);
      this.style = {fontSize: '1.1rem'}

      this._handleClick = this._handleClick.bind(this);
      this._handleClose = this._handleClose.bind(this);
    }

    _handleClick(){
      this.props.onClick(this.props.id);
    }

    _handleClose(){
      this.props.onClose(this.props.id);
    }

    render(){
      return(
        <>
            <li className="nav-item">
                <a  className={(this.props.applicationActive===this.props.id) ? 'nav-link active' : 'nav-link'}
                    id={this.props.id+"-tab"} 
                    data-toggle="tab" 
                    href={"#"+this.props.id} 
                    role="tab" 
                    aria-controls={this.props.id} 
                    aria-selected="true"
                    onClick={this._handleClick}
                  >
                    {this.props.name}
                    &nbsp;<button className="close" type="button" title="Remove this page" style={this.style} onClick={this._handleClose}>×</button>
                </a>
            </li>
        </>
      );
    }
  }