import React, {Component} from 'react';

export class Form extends Component{
    
    constructor(props){
      super(props);
      this.style = {marginTop: 10,borderRadius: '0rem',}
      this._handleClose = this._handleClose.bind(this);
    }

    _handleClose(){
      this.props.onClose(this.props.id);
    }

    render(){
        console.log("Form props: ", this.props);
        
      return(
        <>
            <div 
                    className={(this.props.applicationActive===this.props.id) ? 'tab-pane fade show active' : 'tab-pane fade show'}
                    id={this.props.id} 
                    role="tabpanel" 
                    aria-labelledby={"form"+this.props.id+"-tab"}
            >
                {this.props.content}
                &nbsp;<button className="btn btn-outline-info btn-xs" type="button" title="Cerrar Formulario" style={this.style} onClick={this._handleClose}><i className='fa fa-times' ></i>&nbsp;Cerrar</button>
            </div>
        </>
      );
    }
  }