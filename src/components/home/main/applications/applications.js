import React, {Component} from 'react';
//import $ from 'jquery';

import {ApplicationsTabs} from './applicationstabs' 
import { ApplicationsForms } from './applicationsforms';

export class Applications extends Component{
    /*
    constructor(props){
      super(props);
    }
    */
    render(){
      //console.log("3. Main props: ",this.props);
      return(
        <>
          <ApplicationsTabs
                appTabs={this.props.appTabs} 
                appForms={this.props.appForms} 
                applicationActive={this.props.applicationActive}
                _handleClickTab={this.props._handleClickTab}
                _handleCloseTab={this.props._handleRemoveApplication}
          />
          <ApplicationsForms 
                appForms={this.props.appForms} 
                applicationActive={this.props.applicationActive}
                _handleCloseForm={this.props._handleRemoveApplication}
          />
        </>
      );
    }
  }