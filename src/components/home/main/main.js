import React, {Component} from 'react';
import { connect } from 'react-redux';
import { menuActions } from '../../_actions';

import {UIMenu} from './uimenu/uimenu';
import {Applications} from './applications/applications' 
//import { ClinicaFormsRoute } from '../../forms/clinica';
import { ContabilidadFormsRoute } from '../../forms/contabilidad';

class Main extends Component{
    constructor(props){
      super(props);

      this.state =  {
                    appTabs: [{id: 0, name: "Inicio"}, {id: 1, name: "Moneda"}, {id: 2, name: "Registro de Obligaciones"}], 
                    applicationActive: 0,
                    //appForms: [{id: 0, content: ClinicaFormsRoute.show(0)}],
                    appForms: [{id: 0, content: ContabilidadFormsRoute.show(0,this.props.authentication)}
                              ,{id: 1, content: ContabilidadFormsRoute.show(1,this.props.authentication)}
                              ,{id: 2, content: ContabilidadFormsRoute.show(2,this.props.authentication)}]
                    };

      this.addApplicationTab = this.addApplicationTab.bind(this);
      this.removeApplicationTab = this.removeApplicationTab.bind(this);
      this._handleClickTab = this._handleClickTab.bind(this);
    }

    addApplicationTab(app){
      const app_tabs = this.state.appTabs;
      const app_forms = this.state.appForms;

      const pos = app_tabs.map(function(e) { return e.id; }).indexOf(app.id);

      if(pos===-1){
        app_tabs.push({id: app.id, name: app.name});
        //app_forms.push({id: app.id, content: ClinicaFormsRoute.show(app.id)});
        console.log("FORM_CONTABILIDAD: "+app.id)
        console.log("this.props.authentication: ",this.props.authentication)
        app_forms.push({id: app.id, content: ContabilidadFormsRoute.show(app.id,this.props.authentication)});
      }

      this.setState({appTabs: app_tabs, applicationActive: app.id, appForms: app_forms});
    }

    removeApplicationTab(p_app_id){
      console.log("removeApplicationTab",p_app_id)
      let v_tabs = [];
      this.state.appTabs.map(function(row,index){
        if(row.id!=p_app_id){
          v_tabs.push(row);
        }
      });
      console.log("v_tabs",v_tabs);
      let v_forms = [];
      this.state.appForms.map(function(row,index){
        if(row.id!=p_app_id){
          v_forms.push(row);
        }
      });
      console.log("v_forms",v_forms);

      this.setState({appTabs: v_tabs, appForms: v_forms});
      this._handleClickTab(0);
    }

    _handleClickTab(id){
      this.setState({applicationActive: id});
    }

    render(){
      return(
        <main>
          <UIMenu
                addApplicationTab={this.addApplicationTab}
                moduleUiMenu={this.props.menu.moduleUiMenu}
                moduleSections={this.props.menu.moduleSections}
                menuModuleActive={this.props.menu.menuModuleActive}
                menuNameModuleActive={this.props.menu.menuNameModuleActive}
          />
          <Applications
                appTabs={this.state.appTabs}
                applicationActive={this.state.applicationActive}
                appForms={this.state.appForms}
                _handleClickTab={this._handleClickTab}
                _handleRemoveApplication={this.removeApplicationTab}
          />
        </main>
      );
    }
  }

  function mapStateToProps(state) {
      const menu = state.menu;
      const authentication = state.authentication;
      return {menu,authentication};
  }
  
  const mapDispatchToProps = {
    setModuleUIMenu: menuActions.setModuleUIMenu
  };
  
  const connectMain = connect(mapStateToProps, mapDispatchToProps)(Main);
  export { connectMain as Main };