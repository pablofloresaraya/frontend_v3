import React from 'react';

export function DropdownMenu(props){
  return(
      <div className="dropdown-menu" aria-labelledby="navbarDropdown">{ props.children }</div>
  )
}