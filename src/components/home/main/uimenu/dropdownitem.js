import React from 'react';

export function DropdownItem(props){
  function _onClick(){
      props._handleOnClickApplication({id: props.id, name: props.name});
  }
  return(
      <a className={"dropdown-item"} href="/#" onClick={_onClick}>{props.name}</a>
  )
}