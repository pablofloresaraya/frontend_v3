import React, {Component} from 'react';
import { connect } from 'react-redux';
import { menuActions, userActions } from '../_actions';

import {HomeView} from './homeview'

class Home extends Component{
    constructor(props){
      super(props);
      this._handleOnClickModule = this._handleOnClickModule.bind(this);
    }

    _handleOnClickModule(p_id,p_name) {
        this.props.showModule(p_id, p_name);
    }

    render(){
      console.log("1. Home", this.props);

      return (
        <HomeView
          menuModuleActive={this.props.menuModuleActive}
          menuNameModuleActive={this.props.menuNameModuleActive}
          menuShowModule={this.props.showModule}
          _handleOnClickModule={this._handleOnClickModule}
          _handleOnChangeCompanyUsed={this.props.changeCompanyUsed}
        />
      );
    }
}

function mapStateToProps(state) {
    const menu = state.menu;
    const auth = state.authentication
    return {menu,auth};
}

const mapDispatchToProps = {
  showModule: menuActions.showModule,
  changeCompanyUsed: userActions.useCompany
};

const connectHome = connect(mapStateToProps, mapDispatchToProps)(Home);
export { connectHome as Home };