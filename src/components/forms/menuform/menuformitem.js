import React from 'react';

export function MenuFormItem(props){
    //function _handleOnMouseEnter(e){
    //    props.setStateHover(true,props.index);
    //}
    //function _handleOnMouseLeave(e){
    //    props.setStateHover(false,props.index);
    //}
    function setClassNameA(){
        let v_clase=!props.disabled ? 'btn btn-outline-info btn-xs' : 'btn btn-outline-secondary btn-xs disabled';
        return v_clase;
    }
    function setClassNameI(){
        let v_clase='';
        if(props.type==='NUEVO') v_clase = 'fa fa-file';
        else if(props.type==='GUARDAR') v_clase = 'fa fa-save';
        else if(props.type==='CONSULTAR') v_clase = 'fa fa-search';
        else if(props.type==='ANULAR') v_clase = 'fa fa-ban';
        else if(props.type==='ELIMINAR') v_clase = 'fa fa-trash';
        else if(props.type==='LIMPIAR') v_clase = 'fa fa-undo';
        else if(props.type==='IMPRIMIR') v_clase = 'fa fa-print';
        else if(props.type==='CERRAR') v_clase = 'fa fa-times';
        else if(props.type==='EXPORTAR_PDF') v_clase = 'fas fa-file-pdf';
        else if(props.type==='EXPORTAR_EXCEL') v_clase = 'fas fa-file-excel';

        return v_clase;
    }
    function setTitle(){
        let v_title='';
        if(props.type==='NUEVO') v_title = 'Nuevo';
        else if(props.type==='GUARDAR') v_title = 'Guardar';
        else if(props.type==='CONSULTAR') v_title = 'Consultar';
        else if(props.type==='ANULAR') v_title = 'Anular';
        else if(props.type==='ELIMINAR') v_title = 'Eliminar';
        else if(props.type==='LIMPIAR') v_title = 'Limpiar';
        else if(props.type==='IMPRIMIR') v_title = 'Imprimir';
        else if(props.type==='CERRAR') v_title = 'Cerrar';
        else if(props.type==='EXPORTAR_PDF') v_title = 'Exportar Pdf';
        else if(props.type==='EXPORTAR_EXCEL') v_title = 'Exportar Excel';

        return v_title;
    }
    
    function styleMenuFormItem(){

        let style_item = {
            borderRadius: '0rem',
            backgroundColor: null,
            marginLeft: '5px',
        };
        
        return style_item;
    }

    function _handleOnClick(event){
        props.onClick(props.type);
    }

    return(
        <a className={setClassNameA()} href="/#" 
            style={styleMenuFormItem()} 
            //onMouseEnter={(e) => _handleOnMouseEnter(e)}
            //onMouseLeave={(e) => _handleOnMouseLeave(e)}
            onClick={(e) => _handleOnClick(e)}
        >
            <i className={setClassNameI()} title={setTitle()}></i>&nbsp;{setTitle()}
        </a>
    ) 
}