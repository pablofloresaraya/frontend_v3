import React, {Component} from 'react';
import {MenuFormItem} from './menuformitem'

export class MenuForm extends Component{

    constructor(props){ 
        super(props);

        this.style_menu_form = { 
            marginBottom: '5px', 
        };
       
        //this.state = {menuitems: this.props.menuitems, mousehover: false,}

    }

    /*
    componentDidMount(){
        const items = this.props.menuitems.map((row,index) =>{
            let item = {};
            if(row.type==='NUEVO')
                item = {type: row.type, onClick: row.onClick, title: 'Nuevo',mousehover: false, disabled: row.disabled};
            if(row.type==='GUARDAR')
                item = {type: row.type, onClick: row.onClick, title: 'Guardar',mousehover: false, disabled: row.disabled};
            if(row.type==='CONSULTAR')
                item = {type: row.type, onClick: row.onClick, title: 'Consultar',mousehover: false, disabled: row.disabled};
            if(row.type==='ANULAR')
                item = {type: row.type, onClick: row.onClick, title: 'Anular',mousehover: false, disabled: row.disabled};
            if(row.type==='ELIMINAR')
                item = {type: row.type, onClick: row.onClick, title: 'Eliminar',mousehover: false, disabled: row.disabled};
            
            //console.log("item",item);
            return item;
        });
        //console.log("items",items);
        this.setState({menuitems: items})
        //this.setState({menuitems: this.props.menuitems})
    }
    */

    setStateHover(p_hover,p_index){
        const items = this.state.menuitems.map((row,index) =>{
            if(index===p_index){
                row.mousehover = p_hover;
            }
            return row;
        });
        this.setState({menuitems: items})
    }
    
    _handleOnClick(p_type){
        //console.log("p_type",p_type)
        //const items = this.state.menuitems.map((row,index) =>{
            /*
            if(p_type==='NUEVO'){
                if(row.type==='NUEVO') row.disabled = true;
                if(row.type==='GUARDAR') row.disabled = false;
                if(row.type==='ANULAR') row.disabled = false;
                if(row.type==='ELIMINAR') row.disabled = false;
            }      
            else if(p_type==='GUARDAR'){
                if(row.type==='NUEVO') row.disabled = false;
                if(row.type==='ANULAR') row.disabled = false;
                if(row.type==='ELIMINAR') row.disabled = false;
            }
            */
        //    if(p_type===row.type) row.onClick();
        //    return row;
        //});
        //this.setState({menuitems: items})
    }

    render(){
        
        const items = this.props.menuitems.map((row,index) => (
            <MenuFormItem 
                key={index} 
                type={ row.type } 
                index={index} 
                title={row.title} 
                //setStateHover={this.setStateHover.bind(this)} 
                //onClick={this._handleOnClick.bind(this)}//{row.onClick}
                onClick={row.onClick}
                disabled={row.disabled}
            />
        ));
        return(
            <div style={this.style_menu_form}>
                {items}
            </div>
        );
    }
}