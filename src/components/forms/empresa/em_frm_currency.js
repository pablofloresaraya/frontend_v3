import React, {Component} from 'react';
import $ from 'jquery';
import { config } from '../../_constants';
import {authHeader, base64EncodeUnicode} from '../../_helpers';
import {MenuForm} from '../menuform/menuform';
import {ButtonAddCurrency} from '../querys/modalAddCurrency/ButtonAddCurrency';
import {ButtonAddChangeValue} from '../querys/modalAddChangeValue/ButtonAddChangeValue';
import { Form } from 'react-bootstrap';
import { resolveModuleName } from 'typescript';

const initialStateAlert = {  show:false, message:"" };
const initialStateCurrency = { nombre:'', valor:'', descripcion:'' };
const initialStateChangeValue = { fecha:'', valor:'' };

export class EmFrmCurrency extends Component{

    constructor(props){ 

        super(props);

        this.state = {
            currency:initialStateCurrency,
            valuechange:initialStateChangeValue,
            alert:initialStateAlert,
            monedaitems: [],
            checkBoxes: [],
            checkBoxesDisable: [],
            valuechangeitems: [],
            change_value_id: null
        }

        this.handleClickCheck             = this.handleClickCheck.bind(this);
        this._handleDeleteCurrency        = this._handleDeleteCurrency.bind(this);
        this._handleShowConfirm           = this._handleShowConfirm.bind(this);
        this._handleAcceptConfirm         = this._handleAcceptConfirm.bind(this);
        this._handleDeleteChangeValue     = this._handleDeleteChangeValue.bind(this);
        this._handleShowModalCurrency     = this._handleShowModalCurrency.bind(this);
        this._handleCloseModalCurrency    = this._handleCloseModalCurrency.bind(this);
        this._handleShowModalChangeValue  = this._handleShowModalChangeValue.bind(this);
        this._handleCloseModalChangeValue = this._handleCloseModalChangeValue.bind(this);
        this._handleClickSaveCurrency     = this._handleClickSaveCurrency.bind(this);
        this._handleClickSaveChangeValue  = this._handleClickSaveChangeValue.bind(this);
        this._handleChangeValuesCurrency  = this._handleChangeValuesCurrency.bind(this);
        this._handleChangeValue           = this._handleChangeValue.bind(this);
        this._handleChangeValuesNum       = this._handleChangeValuesNum.bind(this);
        this._handleCloseAlertModal       = this._handleCloseAlertModal.bind(this);
        
    }

    /*componentWillMount() {
        const menuitems = [ 
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        
        this.setState({menuitemss: menuitems});
    }*/

    _handleChangeValuesCurrency(e){

        const { name, value } = e.target;        

        this.setState({
            currency: {
                ...this.state.currency,
                [name] : value
            }          
        })

    }

    _handleChangeValue(e){

        const { name, value } = e.target;        

        this.setState({
            valuechange: {
                ...this.state.valuechange,
                [name] : value
            }          
        })

    }

    _handleChangeValuesNum(e){

        const { name, value } = e.target;
        
        const removeNonNumeric = num => num.toString().replace(/[^0-9]/g, "");

        this.setState({
            valuechange: {
                ...this.state.valuechange,
                [name] : removeNonNumeric(value)
            }          
        })

    }

    _handleClickSaveCurrency(){

        this.setState({ alert: initialStateAlert });

        if(this.state.currency.nombre===""){
            this.setState({
                alert: {
                    ...this.state.alert,
                    show : true,
                    message : "Ingrese Nombre"
                }          
            })            
            return false;
        }

        if(this.state.currency.valor===""){
            this.setState({
                alert: {
                    ...this.state.alert,
                    show : true,
                    message : "Seleccione un Tipo de Valor"
                }          
            })           
            return false;
        }

        if(this.state.currency.descripcion===""){
            this.setState({
                alert: {
                    ...this.state.alert,
                    show : true,
                    message : "Ingrese Descripcion"
                }          
            })     
            return false;
        }

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("currency");
        let v_option = base64EncodeUnicode("2");
        
        const requestOptions = { 
            method: 'POST',
            body: JSON.stringify( this.state.currency )
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const resp = await response.json();
            if(resp.status==='ok'){
                this.getData();
                this._handleCloseModalCurrency();
            }else{                
                this.setState({
                    alert: {
                        ...this.state.alert,
                        show : true,
                        message : resp.errores[0]
                    }          
                })   
                return false;
            }                              
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }
  
    handleClickCheck(e){

        if(e.target.checked){
            const array =  this.state.checkBoxes;
            array.push(e.target.name) //id
            this.setState({ checkBoxes:array })
        }else{
            const array =  this.state.checkBoxes;
            const index = array.indexOf(e.target.name); //id
            array.splice(index,1);            
            this.setState({ checkBoxes:array })
        }
        
    }

    getData(){
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("currency");
        let v_option = base64EncodeUnicode("1");
        
        const requestOptions = { 
            method: 'GET',
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const result = await response.json();
            this.setState({ monedaitems:result.data });                                        
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    getDataValueChange(){
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("currency");
        let v_option = base64EncodeUnicode("4");
        
        const requestOptions = { 
            method: 'GET',
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const result = await response.json();
            this.setState({valuechangeitems:result.data});                                
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    componentDidMount(prevProps){
        
        // Uso tipico (no olvides de comparar los props):
        //if (this.props.userID !== prevProps.userID) {
          this.getData();
          this.getDataValueChange();
        //}
    }

    _handleDeleteCurrency(){
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("currency");
        let v_option = base64EncodeUnicode("3");
        
        const requestOptions = { 
            method: 'POST',
            body: JSON.stringify( this.state.checkBoxes )
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const result = await response.json();
            this.getData();
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    _handleDeleteChangeValue(){ 
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("currency");
        let v_option = base64EncodeUnicode("5");
        
        const requestOptions = { 
            method: 'POST',
            body: JSON.stringify({ id:this.state.change_value_id })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const result = await response.json();
            this.getDataValueChange();
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    _handleShowConfirm(id){

        if(id !== null && this.state.checkBoxes.length===0){ 
            this.setState({ change_value_id:id });            
        }
        $(document.getElementById("ConfirmCurrency")).modal("show");

    }

    _handleShowModalCurrency(){
        this.setState({currency:initialStateCurrency,alert:initialStateAlert});
        $(document.getElementById("addCurrency")).modal("show");
    }

    _handleCloseModalCurrency(){
        this.setState({currency:initialStateCurrency,alert:initialStateAlert});        
        $(document.getElementById("addCurrency")).modal("hide");        
    }

    _handleShowModalChangeValue(){
        this.setState({valuechange:initialStateChangeValue,alert:initialStateAlert});
        $(document.getElementById("addChangeValue")).modal("show");
    }

    _handleCloseModalChangeValue(){            
        this.setState({valuechange:initialStateChangeValue,alert:initialStateAlert});
        $(document.getElementById("addChangeValue")).modal("hide");        
    }

    _handleCloseAlertModal(){            
        this.setState({alert:initialStateAlert});       
    }    

    _handleClickSaveChangeValue(){
        
        this.setState({alert:initialStateAlert});

        /*if(this.state.valuechange.fecha===""){
            this.setState({
                alert: {
                    ...this.state.alert,
                    show : true,
                    message : "Ingrese Fecha"
                }          
            });                   
            return false;
        }

        if(this.state.valuechange.valor===""){
            this.setState({
                alert: {
                    ...this.state.alert,
                    show : true,
                    message : "Ingrese Valor"
                }          
            });            
            return false;
        }*/

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("currency");
        let v_option = base64EncodeUnicode("6");
        
        const requestOptions = { 
            method: 'POST',
            body: JSON.stringify( {datos:this.state.valuechange,moneda:this.state.checkBoxes[0]} )
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const resp = await response.json();            
            if(resp.status==='ok'){
                this.getDataValueChange();
                this._handleCloseModalChangeValue();
            }else{                
                this.setState({
                    alert: {
                        ...this.state.alert,
                        show : true,
                        message : resp.errores[0]
                    }          
                })     
                return false;                
            }                            
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    }

    _handleAcceptConfirm(){

        (this.state.change_value_id !== null && this.state.checkBoxes.length===0)
        ? this._handleDeleteChangeValue()
        : this._handleDeleteCurrency();
        $(document.getElementById("ConfirmCurrency")).modal("hide");

    }

    render(){

        return(            
            <>
                <div className="row" >
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <strong>Moneda</strong>
                            </div>
                            <div className="card-body">
                                
                                <div className="row">
                                    <table className="table table-bordered table-sm">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Nombre</th>
                                                <th>Principal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.monedaitems.map((row,index) => {
                                                return( 
                                                    <tr key={row.id}>
                                                        <td className="text-center"> <input type="checkbox" name={row.id} onChange={this.handleClickCheck} disabled={ row.id != this.state.checkBoxes[0] && this.state.checkBoxes.length } /> </td>
                                                        <td>{row.nombre}</td>
                                                        <td>{row.principal}</td>
                                                    </tr>
                                                )
                                            }
                                        )}    
                                        </tbody>        
                                    </table>
                                </div>
                                
                                <div className="row">
                                    <button className="btn btn-outline-info btn-sm" type="button" onClick={this._handleShowModalCurrency}>
                                        <i className="fas fa-plus-circle"></i> Crear Nuevo
                                    </button>
                                    &nbsp;
                                    <button className="btn btn-outline-info btn-sm" type="button" disabled={this.state.checkBoxes.length===0} onClick={() => this._handleShowConfirm(null)}>
                                        <i className="fas fa-trash-alt"></i> Eliminar
                                    </button>
                                </div>
                                
                            </div>                        
                        </div>  
                    </div>     
                </div>

                <div className="row" >
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <strong>Valor de Cambio</strong>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <table className="table table-bordered table-sm">
                                        <thead>
                                            <tr>                                                
                                                <th>Fecha</th>
                                                <th>Valor</th>
                                                <th>&nbsp;</th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.valuechangeitems.map((row) => {
                                                return( 
                                                    <tr key={row.id}>                                                        
                                                        <td>{row.fecha}</td>
                                                        <td>{row.valor}</td>
                                                        <td>
                                                            <button className="btn btn-outline-danger btn-xs" type="button" onClick={() => this._handleShowConfirm(row.id)}>
                                                                <i className="fas fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                        )}   
                                        </tbody>        
                                    </table>
                                </div>
                                
                                <div className="row">
                                    <button className="btn btn-outline-info btn-sm" type="button" onClick={this._handleShowModalChangeValue} disabled={this.state.checkBoxes.length===0}>
                                        <i className="fas fa-plus-circle"></i> Crear Nuevo
                                    </button>
                                </div>
                            </div>                        
                        </div>  
                    </div>     
                </div>

                <div className="modal fade" tabIndex="-1" role="dialog" id={"ConfirmCurrency"}>
                    <div className="modal-dialog modal-md" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Confirmar</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">                                       
                            <span>Est&aacute; seguro de eliminar el registro?</span>
                        </div>
                        <div className="modal-footer">                            
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this._handleAcceptConfirm}>Aceptar</button>                         
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                        </div>
                    </div>
                </div>

                <div className="modal" tabIndex="-1" role="dialog" id={"addCurrency"}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Crear Tipo Moneda</h5>
                                <button type="button" className="close" data-dismiss="modal" onClick={this._handleCloseModalCurrency} aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            {this.state.alert.show && <div className="alert alert-danger" role="alert">
                                {this.state.alert.message}
                                <button type="button" className="close" onClick={this._handleCloseAlertModal} aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>}
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <form name="currencyForm" className="form-horizontal" style={{marginTop:"1rem"}}> 
                                            <div className="form-group row">
                                                <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Nombre</label>
                                                <div className="col-md-11 col-lg-10">
                                                    <input name="nombre" type="text" value={this.state.currency.nombre} onChange={this._handleChangeValuesCurrency} className="form-control form-control-xs input-sm text-left" placeholder="Nombre" />
                                                </div>
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Tipo Valor</label>
                                                <div className="col-md-11 col-lg-10">
                                                    <select name="valor" value={this.state.currency.valor} onChange={this._handleChangeValuesCurrency} className="form-control form-control-xs input-sm">
                                                        <option >Seleccione</option>
                                                        <option value="DAILY_VALUE">Diario</option>
                                                        <option value="MONTHLY_VALUE">Mensual</option>
                                                        <option value="ANNUAL_VALUE">Anual</option>
                                                    </select>
                                                </div>    
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Descripci&oacute;n</label>
                                                <div className="col-md-11 col-lg-10">
                                                    <textarea name="descripcion" value={this.state.currency.descripcion} onChange={this._handleChangeValuesCurrency} style={{resize:"none"}} className="form-control form-control-xs input-sm text-left" rows="4" cols="50">
                                                    </textarea> 
                                                </div>
                                            </div>                              
                                        </form>
                                    </div>
                                </div>
                            </div>                    
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary" onClick={this._handleClickSaveCurrency} >Crear</button>
                                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this._handleCloseModalCurrency} >Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal" tabIndex="-1" role="dialog" id={"addChangeValue"} >
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Crear Valor de Cambio</h5>
                                <button type="button" className="close" data-dismiss="modal" onClick={this._handleCloseModalChangeValue} aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            {this.state.alert.show && <div className="alert alert-danger" role="alert">
                                {this.state.alert.message}
                                <button type="button" className="close" onClick={this._handleCloseAlertModal} aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>}
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <form name="currencyForm" className="form-horizontal" style={{marginTop:"1rem"}}> 
                                            <div className="form-group row">
                                                <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Fecha</label>
                                                <div className="col-md-11 col-lg-10">
                                                    <Form.Control name="fecha" type="date" className="form-control form-control-xs" value={this.state.valuechange.fecha} onChange={this._handleChangeValue} />
                                                </div>
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Valor</label>
                                                <div className="col-md-11 col-lg-10">
                                                    <input name="valor" type="text" value={this.state.valuechange.valor} onChange={this._handleChangeValuesNum} className="form-control form-control-xs input-sm text-left" />
                                                </div>    
                                            </div>                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary" onClick={this._handleClickSaveChangeValue} >Crear</button>
                                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this._handleCloseModalChangeValue} >Cerrar</button>
                            </div>                    
                            
                        </div>
                    </div>
                </div>

            </>
        )
    }

}