import React, {Component} from 'react';
import $ from 'jquery';
import { config } from '../../_constants';
import {authHeader, base64EncodeUnicode} from '../../_helpers';
import { Form } from 'react-bootstrap';
import {ButtonSupplierList} from '../querys/modalsupplierlist/ButtonSupplierList';
import {ButtonDocumentType} from '../querys/modalDocumentType/ButtonDocumentType';
import {ButtonCostCenter} from '../querys/modalCostCenter/ButtonCostCenter';
import {ButtonCurrencyType} from '../querys/modalCurrencyType/ButtonCurrencyType';
import {MenuForm} from '../menuform/menuform';
import {  validate, clean, format, getCheckDigit } from 'rut.js';

const initialState = {
    
    id:'',
    rut:'',
    dig:'',
    partner:'',
    proveedor:'',
    idtdoc:'',
    tdoc:'',
    impuesto:'',
    porcentaje:'',
    documento:'',
    fdocumento:'',
    fvencimiento:'',
    fregistro:'',
    codmoneda:'',
    moneda:'',
    vcambio:'',
    ccosto:'',
    codccosto:'',
    nccosto:'',
    descripcion:'',
    netoorigen:'',
    netolocal:'',
    imptoorigen:'',
    imptolocal:'',
    exentoorigen:'',
    exentolocal:'',
    totalorigen:'',
    totallocal:''

};

export class ApsFrmObligation extends Component{

    constructor(props){ 

        super(props);

        this.state = {
            form: initialState,
            modal:{
                message:''
            },
            menuitemss: [],
            reading:true
        }

        this._handleChange       = this._handleChange.bind(this);
        this._handleChangeNum    = this._handleChangeNum.bind(this);
        this._handleChangeRut    = this._handleChangeRut.bind(this);
        this._handleSetRut       = this._handleSetRut.bind(this);
        this._handleSetCcosto    = this._handleSetCcosto.bind(this);
        this._handleSetMoneda    = this._handleSetMoneda.bind(this);
        this._handleSetTdoc      = this._handleSetTdoc.bind(this);
        this._handShowModal      = this._handShowModal.bind(this);
        this._handleGetObligation = this._handleGetObligation.bind(this);
        this._handleOnClickBtnGuardar = this._handleOnClickBtnGuardar.bind(this);
        this._handleOnClickBtnLimpiar = this._handleOnClickBtnLimpiar.bind(this);
        this._handleOnClickBtnAnular  = this._handleOnClickBtnAnular.bind(this);
        this._handleOnClickBtnNuevo   = this._handleOnClickBtnNuevo.bind(this);
        this._handleChangeSumaOrigen  = this._handleChangeSumaOrigen.bind(this);       
        this._handleChangeEntryDate   = this._handleChangeEntryDate.bind(this);
        this._handleChangeLocal       = this._handleChangeLocal.bind(this);
        this._handleChangeNumDecimal  = this._handleChangeNumDecimal.bind(this);

    }

    _handleChangeNum(event){

        const { name, value } = event.target;
        
        const removeNonNumeric = num => num.toString().replace(/[^0-9]/g, "");

        this.setState({
            form: {
                ...this.state.form,
                [name] : removeNonNumeric(value)
            }             
        })    
        
    }

    escapeRegExp(string) {
        return string.replace(/[0-9]{1,2}[.][0-9]{1,3}/g,'*$&'); // $& significa toda la cadena coincidente
      }
      

    _handleChangeNumDecimal(event){

        const { name, value } = event.target;

        const decimal = /^[0-9]+([,])?([0-9]+)?$/;
        
        if(decimal.test(value) || value===""){
            this.setState({
                form: {
                    ...this.state.form,
                    [name] : value 
                }             
            })        
        }
        
    }

    _handleChangeRut(event){

        const { name, value } = event.target;
        
        const removeNonNumeric = num => num.toString().replace(/[^0-9]/g, "");

        this.setState({
            form: {
                ...this.state.form,
                [name] : removeNonNumeric(value),
                dig: value ? getCheckDigit(removeNonNumeric(value)) : ""
            }             
        })    
        
    }

    _handleChange(event){

        const { name, value } = event.target;

        this.setState({
            form: {
                ...this.state.form,
                [name] : value
            }             
        });
    }

    _handleSetRut(p_row) { 
        this.setState({
            form: {
              ...this.state.form,
              rut : p_row.run,
              dig : p_row.dig,
              proveedor : p_row.nombre,
              partner : p_row.id
            }           
        })
    }

    _handleSetCcosto(p_row) {
        
        this.setState({
            form: {
              ...this.state.form,
              ccosto : p_row.id,
              codccosto: p_row.codigo,
              nccosto: p_row.nombre
            }           
        })

    }

    _handleSetMoneda(p_row) {
       
        this.setState({
            form: {
              ...this.state.form,
              codmoneda : p_row.id,
              moneda    : p_row.nombre
            }           
        })

    }

    _handleSetTdoc(p_row) {
        
        this.setState({
            form: {
              ...this.state.form,
              idtdoc   : p_row.id,
              tdoc     : p_row.nombre,
              impuesto : p_row.impuesto,
              porcentaje : p_row.porcentaje
            }           
        })

    }
    _handleOnClickBtnGuardar(){

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("obligations");
        let v_option = base64EncodeUnicode("3");
        
        const requestOptions = { 
            method: 'POST',
            body:  JSON.stringify( this.state.form )
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const resp = await response.json();

            if(resp.status==='ok'){
                this.setState({reading:true});
                this._handleGetObligation(); 
                this._handShowModal(resp.message[0]);
            }else{     
                this._handShowModal(resp.errores[0]);
            }  

        })
        .catch((error) => {
            console.error('Error:', error);
        });
        
    }

    _handleGetObligation() {

        if(this.state.reading){
        
            let user = JSON.parse(localStorage.getItem('user'));
            let company = JSON.parse(localStorage.getItem('company'));
            let v_company = base64EncodeUnicode(company.id);
            let v_route = base64EncodeUnicode("obligations");
            let v_option = base64EncodeUnicode("4");
            
            const requestOptions = { 
                method: 'POST',
                body:  JSON.stringify({ proveedor : this.state.form.rut, 
                                        partner   : this.state.form.partner, 
                                        idtdoc    : this.state.form.idtdoc,
                                        documento : this.state.form.documento })
            };

            fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const resp = await response.json();
                if(resp.status==="ok"){                                               
                    this.setState({form:resp.data});
                    this.setStateMenuItem("ANULAR");
                }else{
                    this._handShowModal(resp.errores[0]);
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });

        }

    }

    _handleOnClickBtnAnular(){

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("obligations");
        let v_option = base64EncodeUnicode("5");
        
        const requestOptions = { 
            method: 'POST',
            body:  JSON.stringify({ id:this.state.form.id })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const resp = await response.json();
            (resp.status==='ok') ? this.requestAnulation(resp.message[0]) : this._handShowModal(resp.errores[0]);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
        
    }

    _handleChangeEntryDate(event) {
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("103");
        var v_value = event.target.value;
        var v_name  = event.target.name;

        const formData = new FormData();
        formData.append( 
            "var1", v_value
        );
        
        const requestOptions = { 
            method: 'POST',
            body: formData
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const v_response = await response.json();
                let v_isopen = v_response.isopen;
                if(!v_isopen){
                    this._handShowModal("El periodo se encuentra cerrado.");
                    return false;
                }
                else{
                    this.setState({
                        form: {
                            ...this.state.form,
                            [v_name] : v_value
                        }             
                    }) 
                }
            })
			.catch((error) => {
				console.error('Error:', error);
			});

        
    }

    requestAnulation(msg){
        this._handShowModal(msg);
        this.setState({ form:initialState, reading:true });
        this.setStateMenuItem("DEFAULT");
    }

    _handleOnClickBtnNuevo(){
        this.setState({form:initialState,reading:false});
        this.setStateMenuItem("GUARDAR");
    }

    _handleOnClickBtnLimpiar(){        
        this.setState({ form:initialState, reading:true });
        this.setStateMenuItem("DEFAULT");
    }

    setStateMenuItem(p_option){
        let menuitems = [];
        if(p_option==="NUEVO"){
            menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: true},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: false},
                            //{type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar, disabled: false},
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnImprimir, disabled: true},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: true},
                            //{type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false},
                            //{type: 'CERRAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        else if(p_option==="GUARDAR"){
            menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: false},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: false},
                            //{type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar, disabled: false},
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnImprimir, disabled: true},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: true},
                            //{type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar, disabled: false},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false},
                            //{type: 'CERRAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        else if(p_option==="DEFAULT"){
            menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: false},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: true},
                            //{type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar, disabled: false},
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnImprimir, disabled: true},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: true},
                            //{type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: true},
                            //{type: 'CERRAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        else if(p_option==="ANULAR"){
            menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: false},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: true},                            
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnImprimir, disabled: true},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: false},                    
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: true}                            
                        ];
        }
        
        this.setState({menuitemss: menuitems});
    }

    componentWillMount() {
        const menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnLimpiar, disabled: true},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: false},
                            {type: 'IMPRIMIR', onClick: '', disabled: true},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}                            
                        ];
        
        this.setState({menuitemss: menuitems});
    }

    componentDidMount(){
        this.setStateMenuItem("DEFAULT");
    }

    _handleChangeSumaOrigen(){

        let exento = (this.state.form.exentoorigen==='') ? 0 : this.state.form.exentoorigen;
        let neto   = (this.state.form.netoorigen==='')   ? 0 : this.state.form.netoorigen;
        let impto  = (this.state.form.imptoorigen==='')  ? 0 : this.state.form.imptoorigen
        
        this.setState({
            form: {
              ...this.state.form,
              totalorigen : (parseInt(exento)+parseInt(neto)+parseInt(impto))
            }           
        })

    }

    _handleChangeLocal(){

        let netoOrigen   = (this.state.form.netoorigen==='') ? 0 : this.state.form.netoorigen.replace(',','.');
        let exentoOrigen = (this.state.form.exentoorigen==='') ? 0 : this.state.form.exentoorigen.replace(',','.');
        let imptoOrigen  = (this.state.form.imptoorigen==='')  ? 0 : this.state.form.imptoorigen.replace(',','.');

        let netoLocal = ( parseFloat(netoOrigen) * parseFloat((this.state.form.vcambio==='') ? 0 : this.state.form.vcambio.replace(',','.')) );
        let imptoLocal = ( parseFloat(imptoOrigen) * parseFloat((this.state.form.porcentaje==='') ? 0 : this.state.form.porcentaje.replace(',','.')) );
        let exentoLocal = (this.state.form.exentolocal==='') ? 0 : this.state.form.exentolocal.replace(',','.');

        this.setState({
            form: {
            ...this.state.form,
            netolocal : (netoLocal===0) ? '' : netoLocal,
            imptolocal : (imptoLocal===0) ? '' : imptoLocal,
            totalorigen : (parseFloat(exentoOrigen)+parseFloat(netoOrigen)+parseFloat(imptoOrigen)),
            totallocal : (parseFloat(netoLocal)+parseFloat(imptoLocal)+parseFloat(exentoLocal))
            }           
        })

    }

    _handShowModal(message){

        this.setState({
            modal: {
              ...this.state.modal,
              message : message
            }           
        });

        $(document.getElementById("accAlertObligations")).modal("show");

    }

    render(){
        return(
            <>  
                <MenuForm menuitems={this.state.menuitemss} />
                <div className="row" >
                    <div className="col-lg-12 col-md-12">
                        <div className="card">
                            <div className="card-header">
                                <strong>Informaci&oacute;n de la Obligaci&oacute;n</strong>
                            </div>
                            <div className="card-body">
                                <form name="obligationForm" className="form-horizontal" style={{marginTop:"1rem"}}>                      
                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Proveedor</label>                      
                                            <div className="col-md-2">                                     
                                                <input name="rut" type="text" className="form-control form-control-xs text-center" maxLength="8" value={this.state.form.rut} onChange={this._handleChangeRut} placeholder="RUT" />
                                            </div>
                                            <div className="col-md-1">
                                                <div className="input-group input-group-xs">                                                                                                                   
                                                    <input name="dig" type="text" className="form-control form-control-xs text-center" value={this.state.form.dig} style={{maxWidth:"40px"}} disabled />                                       
                                                    <div className="input-group-append">
                                                        <ButtonSupplierList
                                                            modalId="enRutType"
                                                            setValue={this._handleSetRut}                                                    
                                                        />
                                                    </div>
                                                </div>                                              
                                            </div>
                                            <div className="col-md-7">
                                                <input name="proveedor" type="text" className="form-control form-control-xs text-center" value={this.state.form.proveedor} disabled />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Tipo Doc.</label>  
                                            <div className="col-md-2">
                                                <div className="input-group input-group-xs"> 
                                                    <input name="tdoc" type="text" className="form-control form-control-xs text-center" value={this.state.form.tdoc} disabled />
                                                    <div className="input-group-append">
                                                        <ButtonDocumentType
                                                            modalId="enDocType"
                                                            setValue={this._handleSetTdoc}                                                    
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Impuesto</label>  
                                            <div className="col-md-2">
                                                <input name="impuesto" type="text" className="form-control form-control-xs text-center" value={this.state.form.impuesto} disabled />
                                            </div>
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Porcentaje</label>  
                                            <div className="col-md-2">
                                                <input name="porcentaje" type="text" className="form-control form-control-xs text-center" value={this.state.form.porcentaje} disabled />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Documento</label>  
                                            <div className="col-md-2">
                                                <input name="documento" type="text" className="form-control form-control-xs text-center" value={this.state.form.documento} onBlur={this._handleGetObligation} onChange={this._handleChange} />
                                            </div>
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Fecha Documento</label>  
                                            <div className="col-md-2">
                                                <Form.Control name="fdocumento" type="date" className="form-control form-control-xs" value={this.state.form.fdocumento} onChange={this._handleChange} disabled={this.state.reading} />
                                            </div>
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Fecha Vencimiento</label>  
                                            <div className="col-md-2">
                                                <Form.Control name="fvencimiento" type="date" className="form-control form-control-xs" value={this.state.form.fvencimiento} onChange={this._handleChange}  disabled={this.state.reading}/>
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Fecha Registro</label>  
                                            <div className="col-md-2">
                                                <Form.Control name="fregistro" type="date" className="form-control form-control-xs" value={this.state.form.fregistro} onChange={this._handleChangeEntryDate} disabled={this.state.reading} />
                                            </div>
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Moneda</label>  
                                            <div className="col-md-2">
                                                <div className="input-group input-group-xs">
                                                    <input name="moneda" type="text" className="form-control form-control-xs text-center" value={this.state.form.moneda} disabled />
                                                    <div className="input-group-append">
                                                        <ButtonCurrencyType
                                                            modalId="enCurrencyType"
                                                            setValue={this._handleSetMoneda}
                                                            reading={this.state.reading}                                                    
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Valor Cambio</label>  
                                            <div className="col-md-2">
                                                <input name="vcambio" type="text" className="form-control form-control-xs text-center" value={this.state.form.vcambio} onChange={this._handleChangeNumDecimal} disabled={this.state.reading} />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Centro Costo</label>                      
                                            <div className="col-md-2">
                                                <div className="input-group input-group-xs">                                                                                                                   
                                                    <input name="ccosto" type="text" className="form-control form-control-xs text-center" value={this.state.form.codccosto} disabled />                                       
                                                    <div className="input-group-append">
                                                        <ButtonCostCenter
                                                            modalId="enCostCenter"
                                                            setValue={this._handleSetCcosto}
                                                            reading={this.state.reading}                                                     
                                                        />
                                                    </div>
                                                </div>                                              
                                            </div>
                                            <div className="col-md-8">
                                                <input name="nccosto" type="text" className="form-control form-control-xs text-center" value={this.state.form.nccosto} disabled />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Descripci&oacute;n</label>
                                            <div className="col-md-10">
                                                <textarea name="descripcion" style={{resize:"none"}} value={this.state.form.descripcion} onChange={this._handleChange} disabled={this.state.reading} className="form-control form-control-xs input-sm text-left" rows="2" cols="100">
                                                </textarea> 
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label">&nbsp;</label>
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-center"><b>MONEDA ORIGEN</b></label>
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-center"><b>MONEDA LOCAL</b></label>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Monto Neto</label>
                                            <div className="col-md-2 col-lg-2"><input name="netoorigen" type="text" value={this.state.form.netoorigen} onKeyUp={this._handleChangeLocal} onChange={this._handleChangeNumDecimal} disabled={this.state.reading} className="form-control form-control-xs text-center" /></div>
                                            <div className="col-md-2 col-lg-2"><input name="netolocal" type="text" value={this.state.form.netolocal} disabled className="form-control form-control-xs text-center" /></div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Monto Impuesto</label>
                                            <div className="col-md-2 col-lg-2"><input name="imptoorigen" type="text" value={this.state.form.imptoorigen} onKeyUp={this._handleChangeLocal} onChange={this._handleChangeNumDecimal} disabled={this.state.reading} className="form-control form-control-xs text-center" /></div>
                                            <div className="col-md-2 col-lg-2"><input name="imptolocal" type="text" value={this.state.form.imptolocal} disabled className="form-control form-control-xs text-center" /></div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">Monto Exento</label>
                                            <div className="col-md-2 col-lg-2"><input name="exentoorigen" type="text" value={this.state.form.exentoorigen} onKeyUp={this._handleChangeLocal} onChange={this._handleChangeNumDecimal} disabled={this.state.reading} className="form-control form-control-xs text-center" /></div>
                                            <div className="col-md-2 col-lg-2"><input name="exentolocal" type="text" value={this.state.form.exentolocal} onKeyUp={this._handleChangeLocal} onChange={this._handleChangeNumDecimal} disabled={this.state.reading} className="form-control form-control-xs text-center" /></div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-2 col-lg-2 col-form-label col-form-label-xs control-label input-label text-right">TOTAL</label>
                                            <div className="col-md-2 col-lg-2"><input name="totalorigen" type="text" value={this.state.form.totalorigen} className="form-control form-control-xs text-center" disabled /></div>
                                            <div className="col-md-2 col-lg-2"><input name="totallocal" type="text" value={this.state.form.totallocal} className="form-control form-control-xs text-center" disabled /></div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal fade" tabIndex="-1" role="dialog" id={"accAlertObligations"}>
                    <div className="modal-dialog modal-md" role="document">
                        <div className="modal-content">
                            <div className={`modal-header ${this.state.modal.clase}`}>
                                <h5 className="modal-title"><i className="fas fa-exclamation-triangle"></i></h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">                                       
                                <span>{this.state.modal.message}</span>
                            </div>
                            <div className="modal-footer">                            
                                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.handleClose}>Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </>        
        )    
    }

}