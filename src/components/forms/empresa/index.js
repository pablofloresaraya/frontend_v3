import React from 'react';

import { EmFrmCustomersSuppliers } from './em_frm_customers_suppliers';

export const EmpresaFormsRoute = { 
    show
};

function show(idForm,authentication){
    let form = "";
    
    switch(idForm){
        case 21:
            form = <EmFrmCustomersSuppliers authentication={authentication}/>
            break;
    }
    
    return (form);
}
