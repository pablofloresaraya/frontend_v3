import React from 'react';
import { CurrencyForm } from './CurrencyForm';

export function ModalAddCurrency(props){
    console.log('-> '+props.form);
    return(
        <div className="modal" tabIndex="-1" role="dialog" id={props.id} data-backdrop="static" >
            <div className="modal-dialog" role="document">
                <div className="modal-content">                    
                    {
                        <CurrencyForm                              
                            reload={props.reload}                           
                        />
                    }  
                </div>
            </div>
        </div>
    );
    
}

export default ModalAddCurrency;