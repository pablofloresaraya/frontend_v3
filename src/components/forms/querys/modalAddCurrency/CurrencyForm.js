import React, {Component} from 'react';
import $ from 'jquery'; 
import { config } from '../../../_constants';
import { base64EncodeUnicode } from '../../../_helpers';

export class CurrencyForm extends Component{

    constructor(props){
        super(props);
        
        this.state = {
            form:{
                nombre:"",
                valor:"",
                descripcion:""
            },
            modal:{
                show:false,
                message:""
            } 
        };
        this._handleClickSave = this._handleClickSave.bind(this);
        this._handleChangeValues = this._handleChangeValues.bind(this);
        this._handleCloseModal = this._handleCloseModal.bind(this);
        this.getData = this.getData.bind(this);
    }

    _handleChangeValues(e) {
        
        const { name, value } = e.target;        

        this.setState({
            form: {
                ...this.state.form,
                [name] : value
            }          
        })

    }  

    getData(){
        
        /*let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("currency");
        let v_option = base64EncodeUnicode("1");
        
        const requestOptions = { 
            method: 'POST',
            body: JSON.stringify({ provincia:this.props.provincia })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const resp = await response.json();                      
            this.setState({Data:resp.data});
        })
        .catch((error) => {
            console.error('Error:', error);
        });*/
    }
    _handleCloseModal() {
        $(document.getElementById(this.props.modalId)).modal("hide");
        this.setState({
            form: {
                nombre:"",
                valor:"",
                descripcion:""
            }          
        })
    }

    _handleClickSave(){

        if(this.state.form.nombre===""){
            this.setState({modal:{show:true,message:"Ingrese Nombre"}});
            return false;
        }

        if(this.state.form.valor===""){
            this.setState({modal:{show:true,message:"Seleccione un Tipo de Valor"}});
            return false;
        }

        if(this.state.form.descripcion===""){
            this.setState({modal:{show:true,message:"Ingrese Descripcion"}});
            return false;
        }

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("currency");
        let v_option = base64EncodeUnicode("2");
        
        const requestOptions = { 
            method: 'POST',
            body: JSON.stringify( this.state.form )
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const resp = await response.json();
            if(resp.status.ok){
                this.props.reload();
                this._handleCloseModal();
            }else{
                this.setState({modal:{show:true,message:resp.errores[0]}});
                return false;
            }                              
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    componentDidMount(prevProps) {
        console.log("componentDidUpdate")
        // Uso tipico (no olvides de comparar los props):
        //if (this.props.userID !== prevProps.userID) {
          this.getData();
         
        //}
    }

    render(){

        return(        
            <div>
                <div className="modal-header">
                    <h5 className="modal-title">Crear Tipo Moneda</h5>
                    <button type="button" className="close" data-dismiss="modal" onClick={this._handleCloseModal} aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {this.state.modal.show && <div className="alert alert-danger" role="alert">
                    {this.state.modal.message}
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>}
                <div className="modal-body">
                    <div className="row">
                        <div className="col-lg-12">
                            <form name="currencyForm" className="form-horizontal" style={{marginTop:"1rem"}}> 
                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Nombre</label>
                                    <div className="col-md-11 col-lg-10">
                                        <input name="nombre" type="text" value={this.state.form.nombre} onChange={this._handleChangeValues} className="form-control form-control-xs input-sm text-left" placeholder="Nombre" />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Tipo Valor</label>
                                    <div className="col-md-11 col-lg-10">
                                        <select name="valor" value={this.state.form.valor} onChange={this._handleChangeValues} className="form-control form-control-xs input-sm">
                                            <option >Seleccione</option>
                                            <option value="1">Diario</option>
                                            <option value="2">Mensual</option>
                                            <option value="3">Anual</option>
                                        </select>
                                    </div>    
                                </div>

                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Descripci&oacute;n</label>
                                    <div className="col-md-11 col-lg-10">
                                        <textarea name="descripcion" value={this.state.form.descripcion} onChange={this._handleChangeValues} style={{resize:"none"}} className="form-control form-control-xs input-sm text-left" rows="4" cols="50">
                                        </textarea> 
                                    </div>
                                </div>                              
                            </form>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-primary" onClick={this._handleClickSave} >Crear</button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this._handleCloseModal} >Cerrar</button>
                </div>
            </div>  
        )
    }
};

export default CurrencyForm;