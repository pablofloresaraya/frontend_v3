import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import ModalAddCurrency from './ModalAddCurrency'; 
import { isPropertySignature } from 'typescript';

export function ButtonAddCurrency(props){    
    
    function _handleShow(){
        ReactDOM.render(
            <ModalAddCurrency 
                id={props.modalId}
                reload={props.reload}
            />,
            document.getElementById("section_modal"),
            function(){
                $(document.getElementById(props.modalId)).modal("show")
            }
        );
    }  

    return(
        <button className="btn btn-outline-info btn-sm" type="button" onClick={_handleShow}>
            <i className="fas fa-plus-circle"></i> Crear Nuevo
        </button>
    );
    
}

export default ButtonAddCurrency;