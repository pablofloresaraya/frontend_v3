import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';

import ModalSearchSupplier from './ModalSearchSupplier'; 

export function ButtonSupplierList(props){    
  
    function _handleCloseModal(){
        $(document.getElementById(props.modalId)).modal("hide")
    }
    
    function _handleShow(){
        console.log("modal: "+props.modalId)
        ReactDOM.render(
            <ModalSearchSupplier 
                id={props.modalId}
                setValue={props.setValue}
                close={_handleCloseModal}
            />,
            document.getElementById("section_modal"),
            function(){
                $(document.getElementById(props.modalId)).modal("show")
            }
        );
    }  

    return(
        <button className="btn btn-outline-info btn-sm" type="button" onClick={_handleShow}>
            <i className="fas fa-list"></i>
        </button>
    );
    
}

export default ButtonSupplierList;