import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';

import ModalSearchCurrencyType from './ModalSearchCurrencyType'; 

export function ButtonCurrencyType(props){    
  
    function _handleCloseModal(){
        $(document.getElementById(props.modalId)).modal("hide")
    }
    
    function _handleShow(){
        console.log("modal: "+props.modalId)
        ReactDOM.render(
            <ModalSearchCurrencyType 
                id={props.modalId}
                setValue={props.setValue}
                close={_handleCloseModal}
            />,
            document.getElementById("section_modal"),
            function(){
                $(document.getElementById(props.modalId)).modal("show")
            }
        );
    }  

    return(
        <button className="btn btn-outline-info btn-sm" type="button" onClick={_handleShow} disabled={props.reading} >
            <i className="fas fa-list"></i>
        </button>
    );
    
}

export default ButtonCurrencyType;