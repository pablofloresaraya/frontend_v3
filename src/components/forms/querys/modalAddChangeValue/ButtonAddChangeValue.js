import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import ModalAddChangeValue from './ModalAddChangeValue'; 

export function ButtonAddChangeValue(props){

    function _handleShow(){
        
        ReactDOM.render(
            
            <ModalAddChangeValue 
                id={props.modalId}
                reload={props.reload}
                typeMoney={props.typeMoney}
            />,
            document.getElementById("section_modal"),
            function(){
                $(document.getElementById(props.modalId)).modal("show")
            }
        );
    }  

    return(
        <button className="btn btn-outline-info btn-sm" type="button" onClick={_handleShow} disabled={!props.typeMoney.length}>
            <i className="fas fa-plus-circle"></i> Crear Nuevo
        </button>
    );
    
}

export default ButtonAddChangeValue;