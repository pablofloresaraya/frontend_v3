import React, {Component} from 'react';
import $ from 'jquery'; 
import { config } from '../../../_constants';
import { base64EncodeUnicode } from '../../../_helpers';
import { Form } from 'react-bootstrap';

export class ChangeValueForm extends Component{

    constructor(props){
        super(props);

        this.state = {
            form:{
                fecha:"",
                valor:"",
                moneda:props.typeMoney[0]
            },
            modal:{
                show:false,
                message:""
            } 
        };
        
        this._handleClickSave = this._handleClickSave.bind(this);
        this._handleChangeValues = this._handleChangeValues.bind(this);
        this._handleChangeNum = this._handleChangeNum.bind(this);
        this._handleCloseModal = this._handleCloseModal.bind(this);        
       
    }

    _handleChangeValues(e) {
        
        const { name, value } = e.target;        

        this.setState({
            form: {
                ...this.state.form,
                [name] : value
            }          
        })

    }
    
    _handleChangeNum(event){

        const { name, value } = event.target;        
        const removeNonNumeric = num => num.toString().replace(/[^0-9]/g, "");

        this.setState({
            form: {
                ...this.state.form,
                [name] : removeNonNumeric(value)
            }             
        })    
        
    }

    _handleCloseModal() {
        $(document.getElementById(this.props.modalId)).modal("hide");
        //this.setState(this.props.initialState);
        this.setState({
            form: {
                fecha:"",
                valor:""
            },
            modal:{
                show:false,
                message:""
            }           
        })
    }

    _handleClickSave(){
        
        if(this.state.form.fecha===""){
            this.setState({modal:{show:true,message:"Ingrese Fecha"}});
            return false;
        }

        if(this.state.form.valor===""){
            this.setState({modal:{show:true,message:"Ingrese Valor"}});
            return false;
        }

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("currency");
        let v_option = base64EncodeUnicode("6");
        
        const requestOptions = { 
            method: 'POST',
            body: JSON.stringify( this.state.form )
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const resp = await response.json();
            if(resp.status.ok){
                this.props.reload();
                this._handleCloseModal();
            }else{
                this.setState({modal:{show:true,message:resp.errores[0]}});
                return false;
            }                              
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    render(){

        return(        
            <div>
                <div className="modal-header">
                    <h5 className="modal-title">Crear Valor de Cambio</h5>
                    <button type="button" className="close" data-dismiss="modal" onClick={this._handleCloseModal} aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {this.state.modal.show && <div className="alert alert-danger" role="alert">
                    {this.state.modal.message}
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>}
                <div className="modal-body">
                    <div className="row">
                        <div className="col-lg-12">
                            <form name="currencyForm" className="form-horizontal" style={{marginTop:"1rem"}}> 
                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Fecha</label>
                                    <div className="col-md-11 col-lg-10">
                                        <Form.Control name="fecha" type="date" className="form-control form-control-xs" value={this.state.form.fecha} onChange={this._handleChangeValues} />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Valor</label>
                                    <div className="col-md-11 col-lg-10">
                                        <input name="valor" type="text" value={this.state.form.valor} onChange={this._handleChangeNum} className="form-control form-control-xs input-sm text-left" />
                                    </div>    
                                </div>                            
                            </form>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-primary" onClick={this._handleClickSave} >Crear</button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this._handleCloseModal} >Cerrar</button>
                </div>
            </div>  
        )
    }
};

export default ChangeValueForm;