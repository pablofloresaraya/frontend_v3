import React from 'react';
import { ChangeValueForm } from './ChangeValueForm';  

export function ModalAddCurrency(props){

    return(
        <div className="modal" tabIndex="-1" role="dialog" id={props.id} data-backdrop="static" >
            <div className="modal-dialog" role="document">
                <div className="modal-content">                    
                    {
                        <ChangeValueForm                              
                        reload={props.reload}
                        typeMoney={props.typeMoney}
                        />                                       
                    }  
                </div>
            </div>
        </div>
    );
    
}

export default ModalAddCurrency;