import React from 'react';

import { FormInicio } from './forminicio';
import { EmFrmCurrency } from '../empresa/em_frm_currency';
import { ApsFrmObligation } from '../empresa/aps_frm_obligation';
import { CgFrmRegistroContable } from './cg_frm_registro_contable';
import { CgFrmContabilizarPagares } from './cg_frm_contabilizar_pagares';
import { CgFrmContabilizarConvenios } from './cg_frm_contabilizar_convenios';
import { CgFrmLibroDiario } from './cg_frm_libro_diario';
import { CgFrmLibroMayor } from './cg_frm_libro_mayor';
//import { EmFrmCustomersSuppliers } from '../empresa/em_frm_customers_suppliers';

export const ContabilidadFormsRoute = { 
    show
};

function show(idForm,authentication){
    let form = "";
    
    switch(idForm){
        case 0:
            form = FormInicio.show();            
            break;
        case 1:
            form = <EmFrmCurrency authentication={authentication}/>           
            break;
        case 2:
            form = <ApsFrmObligation authentication={authentication}/>           
            break;    
        case 21:
            form = <CgFrmRegistroContable authentication={authentication}/>
            break;
        case 23:
            form = <CgFrmContabilizarPagares authentication={authentication}/>
            break;
        case 26:
            form = <CgFrmLibroDiario authentication={authentication} />
            break;
        case 27:
            form = <CgFrmLibroMayor authentication={authentication} />
            break;
        case 29:
            form = <CgFrmContabilizarConvenios authentication={authentication} />
            break;
        //case 28:
        //    form = <EmFrmCustomersSuppliers authentication={authentication} />
        //    break; 
    }
    
    return (form);
}
