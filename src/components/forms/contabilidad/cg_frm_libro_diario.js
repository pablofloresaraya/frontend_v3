import React, {Component} from 'react';
import $ from 'jquery';
import { config } from '../../_constants';
import { authHeader, base64EncodeUnicode } from '../../_helpers';
import { Form } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import "./css/customDatePickerWidth.css";
import {MenuForm} from '../menuform/menuform';
import es from 'date-fns/locale/es';
registerLocale('es', es)
export class CgFrmLibroDiario extends Component{

    constructor(props){ 

        super(props);

        let v_cuenta = 1;
        let v_usuario = 1;

        this.state = {
            radio: false            
            ,fechaini: ""
            ,fechafin: ""
            ,fechaperiodo:null
            ,disfecha:true
            ,disperiodo:true
            ,message:""
            ,cuenta: v_cuenta
            ,user: v_usuario  
        }

        this.handleChangeRad = this.handleChangeRad.bind(this);
        this.handleChangeFechas = this.handleChangeFechas.bind(this);
        this.handleChangePeriodo = this.handleChangePeriodo.bind(this);
        this.handleReportPdf = this.handleReportPdf.bind(this);
        this.handleReportExcel = this.handleReportExcel.bind(this);
        this.handleShowModal = this.handleShowModal.bind(this);

    }

    handleChangeRad(event){

        const { name, value } = event.target;

        this.setState({            
            ...this.state
            ,[name]:value                     
        });
        
        (this.state.radio==="F")
        ? this.setState({ fechaini: "", fechafin: "" })
        : this.setState({ fechaperiodo: null });        

    }

    handleChangeFechas(event){

        const { name, value } = event.target;

        this.setState({            
            ...this.state.form,
            [name] : value                         
        })

    }

    handleChangePeriodo(fecha){
        this.setState({ fechaperiodo: fecha });
    }

    handleReportExcel(){
        //window.open(`${config.apiUrl}/accReportExcel.php`);
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("7");
        
        const requestOptions = { 
            method: 'POST',
            body:  JSON.stringify({ 
                                    opcion: "E",
                                    fechaini:this.state.fechaini,
                                    fechafin:this.state.fechafin,
                                    periodo:this.state.fechaperiodo,
                                    cuenta:this.state.cuenta  
                                })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const data = await response.json();            
            (!data) ? this.handleShowModal() : window.open(config.apiUrl+data); 
        })
        .catch((error) => {
            console.error('Error:', error);
        });
        
    }

    handleReportPdf(){

        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("7");
        
        const requestOptions = { 
            method: 'POST',
            body:  JSON.stringify({ 
                                    opcion: "P",
                                    fechaini:this.state.fechaini,
                                    fechafin:this.state.fechafin,
                                    periodo:this.state.fechaperiodo,
                                    cuenta:this.state.cuenta 
                                })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const data = await response.json();            
            (!data) ? this.handleShowModal() : window.open(config.apiUrl+data); 
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    }

    handleShowModal(){
        //console.log('No existe info');
        this.setState({ message: "No existe información" });
        $(document.getElementById("accAlertBook")).modal("show");
    }

    render(){

        const menuitems = [ 
            {type: 'IMPRIMIR', onClick: this.handleReportPdf, disabled: false},            
            {type: 'EXPORTAR_EXCEL', onClick: this.handleReportExcel, disabled: false}
            ];

        return(
        <>                
            <div className="row" >
                <div className="col-lg-12">
                    <MenuForm menuitems={menuitems} />
                    <div className="card">
                        <div className="card-header">
                            <strong>Par&aacute;metros del Informe</strong>
                        </div>
                        <div className="card-body">
                            <form name="libroForm" className="form-horizontal" style={{marginTop:"1rem"}}> 
                                <div className="form-group row">
                                    <div className="col-md-1 col-lg-2">
                                        &nbsp;
                                    </div>
                                    <div className="col-md-1 col-lg-3">
                                        <div className="row">
                                            <div className="col-md-1 col-lg-6">
                                                <input name="radio" id="fecha" type="radio" value="F" checked={this.state.radio==="F"} onChange={this.handleChangeRad}  />&nbsp;
                                                <label className="form-check-label col-form-label control-label input-label" htmlFor="fecha">
                                                    Por fecha
                                                </label>
                                            </div>
                                            <div className="col-md-1 col-lg-6">                                       
                                                <input name="radio" id="periodo" type="radio" value="P" checked={this.state.radio==="P"} onChange={this.handleChangeRad} />&nbsp;
                                                <label className="form-check-label col-form-label control-label input-label" htmlFor="periodo">
                                                    Por per&iacute;odo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label control-label-xs input-label text-right">Fecha Inicio</label>
                                    <div className="col-md-2 col-lg-3">
                                        <Form.Control name="fechaini" type="date" className="form-control form-control-xs" value={this.state.fechaini} onChange={this.handleChangeFechas} disabled={this.state.radio!=="F"} />
                                    </div>    
                                    <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Fecha Termino</label>        
                                    <div className="col-md-2 col-lg-3">
                                        <Form.Control name="fechafin" type="date" className="form-control form-control-xs" value={this.state.fechafin} onChange={this.handleChangeFechas} disabled={this.state.radio!=="F"} />
                                    </div>  
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Per&iacute;odo</label>
                                    <div className="col-md-2 col-lg-3 customDatePickerWidth">
                                        <DatePicker
                                            name="fechaperiodo"
                                            locale={'es'}                                      
                                            selected={this.state.fechaperiodo}
                                            onChange={(date) => this.handleChangePeriodo(date)}
                                            dateFormat="MM-yyyy"
                                            showMonthYearPicker
                                            className="form-control form-control-xs"
                                            isClearable                    
                                            disabled={this.state.radio!=="P"}                                                                               
                                        />
                                    </div>
                                </div>
                            </form>
                        </div>
                        &nbsp;    
                    </div>  
                </div>     
            </div>
            
            <div className="modal fade" tabIndex="-1" role="dialog" id={"accAlertBook"}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                    <div className="modal-header modal-header-danger">
                        <h5 className="modal-title"><i className="fas fa-exclamation-triangle"></i></h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">                                       
                        <span>{this.state.message}</span>
                    </div>
                    <div className="modal-footer">                                                                       
                        <button type="button" className="btn btn-light" data-dismiss="modal">Cerrar</button>
                    </div>
                    </div>
                </div>
            </div>
        </>
        )}
}