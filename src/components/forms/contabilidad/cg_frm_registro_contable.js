import { config } from '../../_constants';
//import {authHeader, base64EncodeUnicode} from '../../_helpers';
import {base64EncodeUnicode} from '../../_helpers';
//import axios from 'axios';
import React, {Component} from 'react';
//import ReactDOM from 'react-dom';
import $ from 'jquery';

import {ButtonSearchVoucherType} from '../querys/modalsearchvouchertype/buttonsearchvouchertype'
import {ButtonSearchAccount} from '../querys/modalsearchaccount/buttonsearchaccount'
import {MenuForm} from '../menuform/menuform'

export class CgFrmRegistroContable extends Component{

    constructor(props){ 
        super(props);

        let v_hoy = new Date();
        let v_fecha = ('0' + v_hoy.getDate()).slice(-2)+'-'+('0' + (v_hoy.getMonth() + 1)).slice(-2)+'-'+v_hoy.getFullYear(); 
        let v_user = this.props.authentication.user;
        let v_company_id = v_user.company_used;

        this.state = {
            general: {id: "", company_id: v_company_id, entry_code: "", entry_year: v_hoy.getFullYear(), entry_month: (v_hoy.getMonth()+1), entry_date: v_fecha, description: "", debit: 0, credit: 0, voucher_type_code: "",create_module: "CONTABILIDAD", removed: false},
            detalle: [],
            vouchertype: {},
            nuevo: false,
            uploadFileEntryDetailFile: null,
            user: v_user,
            menuitemss: []
        }

        this._setVoucherType = this._setVoucherType.bind(this);
        this._handleChangeAccountDetailAccount = this._handleChangeAccountDetailAccount.bind(this);
        this._handleChangeAccountDetailAccountInput = this._handleChangeAccountDetailAccountInput.bind(this);
        this._handleChangeAccountDetailDebit = this._handleChangeAccountDetailDebit.bind(this);
        this._handleChangeAccountDetailCredit = this._handleChangeAccountDetailCredit.bind(this);
        this._handleChangeAccountDetailRut = this._handleChangeAccountDetailRut.bind(this);
        this._handleBlurAccountDetailValidarRut = this._handleBlurAccountDetailValidarRut.bind(this);
        this._handleChangeAccountDetailDetalle = this._handleChangeAccountDetailDetalle.bind(this);
        this._handleChangeAccountDetailTodas = this._handleChangeAccountDetailTodas.bind(this);
        this._handleOnClickBtnNuevo = this._handleOnClickBtnNuevo.bind(this);
        this._handleOnClickBtnGuardar = this._handleOnClickBtnGuardar.bind(this);
        this._handleOnClickBtnConsultar = this._handleOnClickBtnConsultar.bind(this);
        this._handleOnClickBtnImprimir = this._handleOnClickBtnImprimir.bind(this);
        this._handleOnClickBtnAnular = this._handleOnClickBtnAnular.bind(this);
        this._handleOnClickBtnEliminar = this._handleOnClickBtnEliminar.bind(this);
        this._handleOnClickBtnLimpiar = this._handleOnClickBtnLimpiar.bind(this);
        //this.getAccountDetailDetalleTodasTrue = this.getAccountDetailDetalleTodasTrue.bind(this);        

        this._handleClickImportEntryDetail = this._handleClickImportEntryDetail.bind(this);
        this._handleAcceptImportEntryDetail = this._handleAcceptImportEntryDetail.bind(this);
        this._handleCloseImportEntryDetail = this._handleCloseImportEntryDetail.bind(this);
        this._handleChangeUploadFileEntryDetailFile = this._handleChangeUploadFileEntryDetailFile.bind(this);
    } 

    getAccountDetailDetalleTodasTrue(){
        var v_detalle = this.state.detalle;
        var v_row = {rut: '', detalle: '', todas: false, nombre: '', partner_id: ''};
        v_detalle.forEach(row => {
            if(row["todas"]){
                v_row = row;
            }
        });
        return v_row;
    }
    
    setLineAccountDetail(){
        var v_detalle = this.state.detalle;
        v_detalle = v_detalle.map((row, index) => {
            row.line = (index+1);
            return row;
        });

        this.setState({detalle: v_detalle});
    }
    
    duplicarAccountDetailDetalle(){
        var v_detalle = this.state.detalle;
        var v_source_index = -1;
        v_detalle.forEach((row, index) => {
            if(v_detalle[index]["todas"]){
                v_source_index = index;
            }
        });

        if(v_source_index>=0){ //existe algo que copiar
            var v_source = v_detalle[v_source_index];
            v_detalle.forEach((row, index) => {
                if(index!==v_source_index){
                    v_detalle[index]["rut"] = v_source["rut"];
                    v_detalle[index]["detalle"] = v_source["detalle"];
                    v_detalle[index]["nombre"] = v_source["nombre"];
                    v_detalle[index]["todas"] = false;
                    v_detalle[index]["partner_id"] = v_source["partner_id"];
                }
            });
            this.setState({detalle: v_detalle});
        }
    }
    
    _setVoucherType(p_data){
        this.setState(
            {
                vouchertype: {id: p_data.id, company_id: p_data.company_id, voucher_type_code: p_data.voucher_type_code
                            , name: p_data.name, description: p_data.description, class_type: p_data.class_type},
            }
        )
        let v_entry = this.state.general;
        v_entry.voucher_type_code = p_data.voucher_type_code;
        this.setState({general: v_entry});
    }
    
    _handleChangeEntryCode(event) {
        let v_entry = this.state.general;
        v_entry.entry_code = event.target.value;
        this.setState({general: v_entry});
    }

    _handleBlurEntryCode(event) {
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("104");

        const formData = new FormData();
        formData.append( "var1", event.target.value);
        formData.append( "var2", this.state.general.entry_year);
        
        const requestOptions = { 
            method: 'POST',
            body: formData
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const v_response = await response.json();
                let v_data = v_response.data;
                if(v_data.existe===true){               
                    let v_entry = v_data.entry;
                    let v_isOpenPeriod = v_data.isOpenPeriod;
                    let v_voucher_type = v_data.voucher_type; 
                    let v_entry_detail = v_data.entry_detail;

                    let v_general = {id: v_entry.id, company_id: v_entry.company_id, entry_code: v_entry.entry_code
                                    , entry_year: v_entry.entry_year, entry_month: v_entry.entry_month, entry_date: v_entry.entry_date_format
                                    , description: v_entry.description
                                    , debit: v_entry.debit, credit: v_entry.credit
                                    , voucher_type_code: v_voucher_type.voucher_type_code, create_module: v_entry.create_module
                                    , removed: v_entry.removed};
                    
                    this.setState({general: v_general}); 
                    this.setState({vouchertype: v_voucher_type}); 

                    let v_detalle = v_entry_detail.map((row, index) => {
                        let v_row =  {entry_id: row.entry_id, line: row.line
                                    , id: row.account_id, code: row.account.account_code, name: row.account.name
                                    , debit: row.debit, credit: row.credit
                                    , rut: row.partner.identifier, detalle: row.description, todas: false
                                    , nombre: row.partner.name, partner_id: row.partner_id};
                        
                        return v_row;
                        
                    });

                    this.setState({detalle: v_detalle});
                    this.setState({nuevo: true});
                    
                    if(v_isOpenPeriod)
                        this.setStateMenuItem("CONSULTAR");
                    else
                        this.setStateMenuItem("CONSULTAR_CERRADO");

                }
            })
			.catch((error) => {
				console.error('Error:', error);
			});
        

    }

    _handleChangeEntryDate(event) {
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("103");
        var v_value = event.target.value;

        const formData = new FormData();
        formData.append( 
            "var1", v_value
        );
        
        const requestOptions = { 
            method: 'POST',
            body: formData
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const v_response = await response.json();
                let v_isopen = v_response.isopen;
                if(!v_isopen){
                    alert("El periodo se encuentra cerrado.");
                    return false;
                }
                else{
                    let v_entry = this.state.general;
                    v_entry.entry_date = v_value;
                    this.setState({general: v_entry});
                }
            })
			.catch((error) => {
				console.error('Error:', error);
			});

        
    }

    _handleChangeVoucherTypeCode(event) {
        let v_entry = this.state.general;
        v_entry.voucher_type_code = event.target.value;
        this.setState({general: v_entry});
    }

    _handleChangeDescription(event) {
        let v_entry = this.state.general;
        v_entry.description = event.target.value;
        this.setState({general: v_entry});
    }

    _handleClickAddRow(event){
        var v_detalle = this.state.detalle;
        var v_line = v_detalle.length;
        var v_row_duplicar_detalle = {rut: '', detalle: '', todas: false};
        if(v_line===0) v_line=1
        else v_line++;
        
        v_row_duplicar_detalle = this.getAccountDetailDetalleTodasTrue();
        v_detalle.push({entry_id: null, line: v_line, id: null, code: '', name: '', debit: 0, credit: 0
                        , rut: v_row_duplicar_detalle.rut, detalle: v_row_duplicar_detalle.detalle, todas: false
                        , nombre: v_row_duplicar_detalle.nombre, partner_id: v_row_duplicar_detalle.partner_id})
        
        this.setState({detalle: v_detalle});        
    }

    _handleChangeAccountDetailAccount(p_line, p_id, p_code, p_name) {
        var v_detalle = this.state.detalle;
        //v_detalle[p_line]["account_id"] = p_value;
        v_detalle[p_line]["id"] = p_id;
        v_detalle[p_line]["code"] = p_code;
        v_detalle[p_line]["name"] = p_name;

        //console.log("p_line",p_line,p_id,p_code,p_name);
        //console.log("v_detalle",v_detalle);
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailAccountInput(p_line, p_code) {
        /*
        let v_id = null;
        let v_name = null;

        let v_detalle = this.state.detalle;
        //v_detalle[p_line]["account_id"] = p_value;
        v_detalle[p_line]["id"] = p_id;
        v_detalle[p_line]["code"] = p_code;
        v_detalle[p_line]["name"] = p_name;

        console.log("p_line",p_line,p_id,p_code,p_name);
        console.log("v_detalle",v_detalle);
        this.setState({detalle: v_detalle});
        */
    }

    _handleChangeAccountDetailDebit(p_line, p_value) {
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["debit"] = p_value;
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailCredit(p_line, p_value) {
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["credit"] = p_value;
        this.setState({detalle: v_detalle});
    }

    _handleBlurAccountDetailValidarRut(p_line, p_value) {
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("partners");
        let v_option = base64EncodeUnicode("101");

        const formData = new FormData();
        formData.append( 
            "var1", 
            p_value
        );
        
        const requestOptions = { 
            method: 'POST',
            body: formData
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const v_response = await response.json();
                let v_data = v_response.data;
                let v_errores = v_response.errores;
                let v_status = v_response.status;
                //let v_message = v_response.message;
                var v_error_message = "";
                let v_detalle = this.state.detalle;
                if(v_status==="error"){
                    v_errores.forEach((error,index) => {
                        if(v_error_message.length===0){
                            v_error_message = error;
                        }
                        else{
                            v_error_message += "\n"+error;
                        }
                    });

                    alert(v_error_message);

                    v_detalle[p_line]["rut"] = "";
                    v_detalle[p_line]["nombre"] = "";
                    v_detalle[p_line]["partner_id"] = "";
                    this.setState({detalle: v_detalle});                    
                }
                else{
                    console.log("v_data",v_data);
                    console.log("v_data.name",v_data.name);
                    v_detalle[p_line]["rut"] = p_value;
                    v_detalle[p_line]["nombre"] = v_data.name;
                    v_detalle[p_line]["partner_id"] = v_data.id;
                    this.setState({detalle: v_detalle});
                }
            })
			.catch((error) => {
				console.error('Error:', error);
			});
    }

    _handleChangeAccountDetailRut(p_line, p_value) {        
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["rut"] = p_value;
        this.setState({detalle: v_detalle});
        ////console.log(this.state.detalle);
    }

    _handleChangeAccountDetailDetalle(p_line, p_value) {
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["detalle"] = p_value;
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailTodas(p_line, p_value) {
        var v_detalle = this.state.detalle;
        var v_existeOtraLineaConTodasTrue = false;
        if(p_value){
            //validar que no exista otra linea con todas = true  
            v_detalle.forEach((row, index) => {
                if(index!==p_line && v_detalle[index]["todas"]===true){
                    v_existeOtraLineaConTodasTrue = true;
                }
            });  
        }
        if(v_existeOtraLineaConTodasTrue){
            alert("Nos es posible realizar el cambio, ya que existe otra línea marca para repetir");
            return false;
        }
        else{
            v_detalle[p_line]["todas"] = p_value;
            this.setState({detalle: v_detalle});
        }
    }

    _handleClickCopyRow(p_index) {
        var v_detalle = this.state.detalle;
        v_detalle.push({entry_id: v_detalle[p_index].entry_id, line: v_detalle[p_index].line, id: v_detalle[p_index].id, code: v_detalle[p_index].code, name: v_detalle[p_index].name, debit: v_detalle[p_index].debit, credit: v_detalle[p_index].credit, rut: v_detalle[p_index].rut, detalle: v_detalle[p_index].detalle, todas: false, nombre: v_detalle[p_index].nombre, partner_id: v_detalle[p_index].partner_id})
        this.setState({detalle: v_detalle});
        this.setLineAccountDetail();
    }

    _handleClickDelRow(p_index) {
        var v_detalle = this.state.detalle;
        v_detalle.splice(p_index, 1);
        this.setState({detalle: v_detalle});
        this.setLineAccountDetail();
    }

    _handleClickImportEntryDetail(){
        
        $(document.getElementById("uploadFileEntryDetail")).modal("show");
    }
    
    _handleAcceptImportEntryDetail(){
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("101");

        const formData = new FormData();
        formData.append( 
            "fileEntryDetail", 
            this.state.uploadFileEntryDetailFile, 
            this.state.uploadFileEntryDetailFile.name
        );
        
        const requestOptions = { 
            method: 'POST',
            //headers: authHeader(),
            body: formData //JSON.stringify({ title: 'React POST Request Example' })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const v_response = await response.json();
                let v_data = v_response.data;
                let v_errores = v_response.errores;
                let v_status = v_response.status;
                //let v_message = v_response.message;
                var v_error_message = "";
                let v_detalle = [];
                if(v_status==="error"){
                    v_errores.forEach((error,index) => {
                        if(v_error_message.length===0){
                            v_error_message = error;
                        }
                        else{
                            v_error_message += "\n"+error;
                        }
                    });
                    console.log("v_error_message",v_error_message);
                    alert("Ocurrieron los siguientes errores al importar el archivo:\n"+v_error_message);
                }
                else{
                    v_detalle = this.state.detalle;
                    v_detalle = v_data.map((row, index) => {
                        let v_row =  {entry_id: null, line: row.line
                                    , id: row.id, code: row.code, name: row.name
                                    , debit: row.debit, credit: row.credit
                                    , rut: row.rut, detalle: row.detalle, todas: false
                                    , nombre: row.nombre, partner_id: row.partner_id};
                        
                        return v_row;
                        
                    });
                }

                this.setState({detalle: v_detalle});
            })
			.catch((error) => {
				console.error('Error:', error);
			});
            
        $(document.getElementById("uploadFileEntryDetail")).modal("hide")
    }
  
    _handleCloseImportEntryDetail(){
        $(document.getElementById("uploadFileEntryDetail")).modal("hide")
    }
    
    _handleChangeUploadFileEntryDetailFile(e){ 
        // Update the state 
        this.setState({ uploadFileEntryDetailFile: e.target.files[0] }); 
    };

    _handleOnClickBtnNuevo(){
        let v_hoy = new Date();
        let v_fecha = ('0' + v_hoy.getDate()).slice(-2)+'-'+('0' + (v_hoy.getMonth() + 1)).slice(-2)+'-'+v_hoy.getFullYear(); 
        let v_entry = this.state.general;
        v_entry.entry_date = v_fecha;
        v_entry.id = "";
        v_entry.entry_code = "";
        v_entry.entry_year = v_hoy.getFullYear();
        v_entry.entry_month = (v_hoy.getMonth()+1);
        v_entry.entry_date = v_fecha;

        this.setState({general: v_entry});
        this.setState({nuevo: true});

        let v_detalle = [{entry_id: null, line: 1, id: null, code: '', name: '', debit: 0, credit: 0, rut: '', detalle: '', todas: false, nombre: '', partner_id: ''}];
        this.setState({detalle: v_detalle});

        this.setStateMenuItem("NUEVO");
    }

    _handleOnClickBtnGuardar(){
        //Realizar validaciones
        let v_entry = this.state.general;
        if(v_entry.entry_date===""){
            alert("La Fecha ingresada no es válida.");
            return false;
        }
        if(v_entry.voucher_type_code===""){
            alert("Debe seleccionar el Tipo de Comprobante.");
            return false;
        }
        if(v_entry.description===""){
            alert("Debe ingresar la Glosa del asiento.");
            return false;
        }
        let v_entry_detail = this.state.detalle;
        if(v_entry_detail.length===0){
            alert("Debe ingresar el Detalle del asiento.");
            return false;
        }
        var tdebito = 0;
        var tcredito = 0;
        var error_detalle = false;
        v_entry_detail.forEach((row,index) => {
            console.log()
            if(row.debit===0 && row.credit===0){
                alert("El Detalle contiene un registro sin valor en el debe y en el haber.");
                error_detalle=true;
            }
            tdebito += row.debit;
            tcredito += row.credit;
        });
        if(error_detalle){
            return false;
        }

        if(tdebito!==tcredito){
            alert("El asiento se encuentra descuadrado.");
            return false;
        }
        
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("102");
        var v_datos = this.state;

        const requestOptions = { 
            method: 'POST',
            //headers: authHeader(),
            body: JSON.stringify(v_datos)
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const v_response = await response.json();                
                
                let v_data = v_response.data;
                let v_errores = v_response.errores;
                let v_status = v_response.status;
                //let v_message = v_response.message;
                var v_error_message = "";
                
                if(v_status==="error"){
                    v_errores.forEach((error,index) => {
                        if(v_error_message.length===0){
                            v_error_message = error;
                        }
                        else{
                            v_error_message += "\n"+error;
                        }
                    });
                    console.log("v_error_message",v_error_message);
                    alert("Ocurrieron los siguientes errores al guardar los registros:\n"+v_error_message);
                }
                else{                    
                    let v_general = this.state.general;
                    v_general.id = v_data.id;
                    v_general.entry_code = v_data.entry_code;
                    this.setState({general: v_general});

                    let v_detalle = this.state.detalle;
                    v_detalle = v_detalle.map((row, index) => {
                        let v_row =  {entry_id: v_data.id, line: row.line
                                    , id: row.id, code: row.code, name: row.name
                                    , debit: row.debit, credit: row.credit
                                    , rut: row.rut, detalle: row.detalle, todas: false
                                    , nombre: row.nombre, partner_id: row.partner_id};
                        
                        return v_row;
                        
                    });

                    this.setState({detalle: v_detalle});
                }
            })
			.catch((error) => {
				console.error('Error:', error);
			});
    }

    _handleOnClickBtnConsultar(){
        console.log("Consultar");
    }

    _handleOnClickBtnImprimir(){
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("106");
        let v_entry_id = this.state.general.id;

        if(v_entry_id===""){
            return false;
        }

        window.open(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}&var1=${v_entry_id}`);
    }

    _handleOnClickBtnAnular(){
        let v_general = this.state.general;
        let v_hoy = new Date();
        let v_fecha = ('0' + v_hoy.getDate()).slice(-2)+'-'+('0' + (v_hoy.getMonth() + 1)).slice(-2)+'-'+v_hoy.getFullYear(); 

        if(v_general.removed){
            alert("No es posible anular un asiento que se encuentra eliminado.");
            return false;
        }

        v_general.description = 'ANULAR ASIENTO: '+v_general.entry_code+' - '+v_general.description;
        v_general.id = "";
        v_general.entry_code = "";
        v_general.entry_year = v_hoy.getFullYear();
        v_general.entry_month = (v_hoy.getMonth()+1);
        v_general.entry_date = v_fecha;

        this.setState({general: v_general});

        let v_detalle = this.state.detalle;
        v_detalle = v_detalle.map((row, index) => {
            let v_row =  {entry_id: "", line: row.line
                        , id: row.id, code: row.code, name: row.name
                        , debit: row.credit, credit: row.debit
                        , rut: row.rut, detalle: row.detalle, todas: false
                        , nombre: row.nombre, partner_id: row.partner_id};
            
            return v_row;            
        });

        this.setState({detalle: v_detalle});

        this.setStateMenuItem("NUEVO");
    }

    _handleOnClickBtnEliminar(){
        
        //Realizar validaciones
        let v_entry = this.state.general;
        if(v_entry.id===""){
            alert("Debe seleccionar un asiento contable antes de realizar la eliminación.");
            return false;
        }

        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("105");
        var v_datos = this.state;

        const requestOptions = { 
            method: 'POST',
            //headers: authHeader(),
            body: JSON.stringify(v_datos)
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const v_response = await response.json();                
                
                //let v_data = v_response.data;
                let v_errores = v_response.errores;
                let v_status = v_response.status;
                //let v_message = v_response.message;
                var v_error_message = "";
                
                if(v_status==="error"){
                    v_errores.forEach((error,index) => {
                        if(v_error_message.length===0){
                            v_error_message = error;
                        }
                        else{
                            v_error_message += "\n"+error;
                        }
                    });
                    console.log("v_error_message",v_error_message);
                    alert("Ocurrieron los siguientes errores al eliminar el asiento contable:\n"+v_error_message);
                }
                else{                    
                    let v_general = this.state.general;
                    v_general.removed = true;
                    this.setState({general: v_general});
                }
            })
			.catch((error) => {
				console.error('Error:', error);
			});
    }

    _handleOnClickBtnLimpiar(){
        let user = JSON.parse(localStorage.getItem('user'));

        let v_hoy = new Date();
        let v_fecha = ('0' + v_hoy.getDate()).slice(-2)+'-'+('0' + (v_hoy.getMonth() + 1)).slice(-2)+'-'+v_hoy.getFullYear(); 
        let v_company_id = user.company_used;

        //this.setState({general: {id: "", company_id: v_company_id, entry_code: "", entry_year: v_hoy.getFullYear(), entry_month: (v_hoy.getMonth()+1), entry_date: v_fecha, description: "", debit: 0, credit: 0, voucher_type_code: "",create_module: "CONTABLE"}});
        
        this.setState({
            general: {id: "", company_id: v_company_id, entry_code: "", entry_year: v_hoy.getFullYear(), entry_month: (v_hoy.getMonth()+1), entry_date: v_fecha, description: "", debit: 0, credit: 0, voucher_type_code: "",create_module: "CONTABLE"},
            detalle: [],
            vouchertype: {},
            nuevo: false,
            uploadFileEntryDetailFile: null
        });
        
        this.setStateMenuItem("DEFAULT");
    }

    setStateMenuItem(p_option){
        let menuitems = [];
        if(p_option==="NUEVO"){
            menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: true},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: false},
                            //{type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar, disabled: false},
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnImprimir, disabled: true},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: true},
                            {type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false},
                            //{type: 'CERRAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        else if(p_option==="CONSULTAR"){
            menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: true},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: false},
                            //{type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar, disabled: false},
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnImprimir, disabled: false},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: false},
                            {type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar, disabled: false},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false},
                            //{type: 'CERRAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        else if(p_option==="CONSULTAR_CERRADO"){
            menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: true},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: true},
                            //{type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar, disabled: false},
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnImprimir, disabled: false},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: false},
                            {type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false},
                            //{type: 'CERRAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        else if(p_option==="GUARDAR"){
            menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: false},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: false},
                            //{type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar, disabled: false},
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnImprimir, disabled: false},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: false},
                            {type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar, disabled: false},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false},
                            //{type: 'CERRAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        else if(p_option==="DEFAULT"){
            menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: false},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: true},
                            //{type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar, disabled: false},
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnImprimir, disabled: true},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: true},
                            {type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false},
                            //{type: 'CERRAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        
        this.setState({menuitemss: menuitems});
    }

    componentWillMount() {
        const menuitems = [ 
                            {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo, disabled: false},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: true},
                            //{type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar, disabled: false},
                            {type: 'IMPRIMIR', onClick: this._handleOnClickBtnGuardar, disabled: true},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular, disabled: true},
                            {type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false},
                            //{type: 'CERRAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        
        this.setState({menuitemss: menuitems});
    }
    
    render(){
        return(
        <>
            <MenuForm menuitems={this.state.menuitemss} />
            <div className="row">
                <div className="col-lg-12">
                <div className="card">
                    <div className="card-header">
                        <strong>Informaci&oacute;n General</strong>
                    </div>
                    <div className="card-body">                    
                        <form>
                            <div className="form-group row">
                                <label htmlFor="inputNumero" 
                                    className="col-md-2 col-lg-1 col-form-label col-form-label-xs">
                                        N&uacute;mero
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <input className="form-control form-control-xs" 
                                        id="inputNumero" 
                                        type="text" 
                                        placeholder=""
                                        value={this.state.general.entry_code}
                                        onChange={this._handleChangeEntryCode.bind(this)} 
                                        onBlur={this._handleBlurEntryCode.bind(this)}
                                        readOnly={this.state.nuevo}
                                        autoComplete="off"
                                    />
                                </div>
                                <label htmlFor="inputUsuario" 
                                    className="col-md-2 col-lg-2 col-form-label col-form-label-xs">
                                        Usuario
                                </label>
                                <div className="col-md-6 col-lg-5">
                                    <input type="text" 
                                        className="form-control-plaintext form-control-xs" 
                                        readOnly
                                        id="inputUsuario" 
                                        value={this.props.authentication.user.name}
                                    />
                                </div>
                                { this.state.general.removed ? <EntryRemoved /> : null }
                            </div> 
                            <div className="form-group row">
                                <label htmlFor="inputFecha" 
                                    className="col-md-2 col-lg-1 col-form-label col-form-label-xs">
                                    Fecha
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <input className="form-control form-control-xs" 
                                        id="inputFecha" 
                                        type="text" 
                                        value={this.state.general.entry_date}
                                        onChange={this._handleChangeEntryDate.bind(this)} 
                                    />
                                    
                                </div>
                                <label htmlFor="inputTipoComprobante" 
                                    className="col-md-2 col-lg-2 col-form-label col-form-label-xs">
                                    Tipo Comprobante
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <div className="input-group input-group-xs">
                                        <input className="form-control py-1" 
                                            id="inputTipoComprobante" 
                                            type="text" 
                                            placeholder=""
                                            value={this.state.general.voucher_type_code}
                                            onChange={this._handleChangeVoucherTypeCode.bind(this)}   
                                        />
                                        <div className="input-group-append">
                                            <ButtonSearchVoucherType 
                                                modalId="cnVoucherType"
                                                setValue={this._setVoucherType}/>
                                        </div>
                                    </div>   
                                </div>                         
                            </div> 
                            <div className="form-group row">
                                <label htmlFor="inputGlosa" 
                                    className="col-md-2 col-lg-1 col-form-label col-form-label-xs">
                                    Glosa
                                </label>
                                <div className="col-md-10 col-lg-11">
                                    <textarea className="form-control form-control-xs" 
                                        id="inputGlosa" 
                                        type="text" 
                                        placeholder=""
                                        value={this.state.general.description}
                                        onChange={this._handleChangeDescription.bind(this)} 
                                    >
                                    </textarea>
                                </div>                        
                            </div> 
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <br />    
            <button className={`btn ${(this.state.nuevo)? "btn-outline-info":"btn-outline-secondary"} btn-xs`} type="button" onClick={this._handleClickImportEntryDetail.bind(this)} disabled={!this.state.nuevo}>
                <i className="fas fa-file-upload"></i>&nbsp;Importar Detalle
            </button> 
            <div className="row">
                <div className="col-lg-12">
                <div className="card">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <strong>Detalle</strong>&nbsp;
                        <button className={`btn ${(this.state.nuevo)? "btn-outline-info":"btn-outline-secondary"} btn-xs`} type="button" onClick={this._handleClickAddRow.bind(this)} disabled={!this.state.nuevo}>
                            <i className="fas fa-plus"></i>&nbsp;Agregar
                        </button>     
                    </div>
                    <div className="card-body">
                        <table className="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th scope="col" style={{width: '50px', textAlign: 'center'}}>L&iacute;nea</th>
                                    <th scope="col" colSpan="2">Cuenta</th>
                                    <th scope="col" style={{width: '130px', textAlign: 'center'}}>Debe</th>
                                    <th scope="col" style={{width: '130px', textAlign: 'center'}}>Haber</th>
                                    <th scope="col" style={{width: '150px', textAlign: 'center'}}>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.detalle.map((row, index) => {
                                        return(  
                                            <AccountDetail 
                                                key={index}
                                                index={index}
                                                line={row.line} 
                                                account_id={row.code}
                                                account_name={row.name}
                                                debit={row.debit}
                                                credit={row.credit}
                                                rut={row.rut}
                                                nombre={row.nombre}
                                                detalle={row.detalle}
                                                todas={row.todas}
                                                onChangeAccountId={this._handleChangeAccountDetailAccount}
                                                onChangeAccountIdInput={this._handleChangeAccountDetailAccountInput}
                                                onChangeDebit={this._handleChangeAccountDetailDebit}
                                                onChangeCredit={this._handleChangeAccountDetailCredit}
                                                onClickCopyRow={this._handleClickCopyRow.bind(this)}
                                                onClickDelRow={this._handleClickDelRow.bind(this)}
                                                onChangeRut={this._handleChangeAccountDetailRut}
                                                onBlurValidarRut={this._handleBlurAccountDetailValidarRut}
                                                onChangeDetalle={this._handleChangeAccountDetailDetalle}
                                                onChangeTodas={this._handleChangeAccountDetailTodas}
                                            />
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
            
            <div className="modal fade" tabIndex="-1" role="dialog" id="uploadFileEntryDetail">
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Importar Archivo</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">                                       
                        <form>
                            <div className="form-group row">
                                <input type="file" 
                                    className="form-control-file" 
                                    id="uploadFileEntryDetail_file" 
                                    onChange={this._handleChangeUploadFileEntryDetailFile}
                                />
                            </div>
                        </form>
                        <div className="row">                            
                                <br />
                                <strong>Caracteristicas del archivo:</strong>
                                <ul>
                                    <li>Tipo de archivo Excel (xlsx)</li>
                                    <li>No debe tener encabezado</li>
                                    <li>Columnas (deben estar en el mismo orden que se indica a continuaci&oacute;n</li>
                                    <ol>
                                        <li>N&uacute;mero de cuenta (num&eacute;rico - obligatorio)</li>
                                        <li>(*) Monto al debe (num&eacute;rico - obligatorio)</li>
                                        <li>(*) Monto al haber (num&eacute;rico - obligatorio)</li>
                                        <li>Rut con gui&oacute;n y digito verificador (alfanum&eacute;rico - no obligatorio)</li>
                                        <li>Texto con el detalle asociado a la l&iacute;nea (alfanum&eacute;rico - no obligatorio)</li>
                                    </ol>
                                </ul>
                                * Si existe un valor distinto de cero al Debe y Haber se crearán dos l&iacute;neas contables.
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this._handleAcceptImportEntryDetail}>Importar</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this._handleCloseImportEntryDetail}>Cancelar</button>
                    </div>
                    </div>
                </div>
            </div>             
        </>
      );
    }
}

function EntryRemoved(props){
    return(
        <div className="col-md-2 col-lg-2">
            <strong style={{color: 'red'}}>ELIMINADO</strong>
        </div>
    )
}
  
function AccountDetail(props){    

    function _setAccount(p_data){
        //console.log("p_data",p_data)
        if(p_data.isLeaf){
            props.onChangeAccountId(props.index, p_data.id, p_data.account_code,p_data.name);
        }
    }

    function _handleChangeAccountId(e){
        //console.log("props.index",props.index);
        props.onChangeAccountIdInput(props.index, e.target.value);
    }

    function _handleChangeDebit(e){
        props.onChangeDebit(props.index, e.target.value);
    }

    function _handleChangeCredit(e){
        props.onChangeCredit(props.index, e.target.value);
    }

    function _handleAcceptAccDetailLine(){
        
        $(document.getElementById(props.modalId)).modal("hide")
    }
  
    function _handleCloseAccDetailLine(){
        $(document.getElementById(props.modalId)).modal("hide")
    }

    function _handleChangeRut(e){
        //console.log("AccountDetail->_handleChangeRut",props.index,e.target.value);
        props.onChangeRut(props.index,e.target.value);
    }

    function _handleValidarRut(e){
        props.onBlurValidarRut(props.index,e.target.value);
    }

    function _handleChangeDetalle(e){
        props.onChangeDetalle(props.index,e.target.value);
    }

    function _handleChangeTodas(e){
        props.onChangeTodas(props.index,e.target.checked);
    }

    function _handleClickEditRow(e){
        $(document.getElementById("accDetailLine"+props.index)).modal("show")
    }

    function _handleClickCopyRow(e){
        props.onClickCopyRow(props.index);
    }

    function _handleClickDelRow(e){
        props.onClickDelRow(props.index);
    }

    return(
        <tr>
            <th scope="row">{props.line}</th>
            <td style={{width: '150px',}}>
                <div className="input-group input-group-xs">
                    <input className="form-control py-1" 
                        id={"inputAccountId"+props.index} 
                        type="text" 
                        placeholder=""
                        value={props.account_id}
                        onChange={_handleChangeAccountId}
                    />
                    <div className="input-group-append">
                        <ButtonSearchAccount 
                            modalId="cnAccount"
                            setValue={_setAccount}/>
                    </div>
                </div> 
            </td>
            <td>
                {props.account_name}
            </td>
            <td>
                <input className="form-control py-1 form-control-xs" 
                        id={"inputDebit"+props.index}
                        type="text" 
                        placeholder=""
                        style={{textAlign: 'right'}}
                        value={props.debit}
                        onChange={_handleChangeDebit}
                />
            </td>
            <td>
                <input className="form-control py-1 form-control-xs" 
                        id={"inputCredit"+props.index}
                        type="text" 
                        placeholder=""
                        style={{textAlign: 'right'}}
                        value={props.credit}
                        onChange={_handleChangeCredit}
                />
            </td>
            <td>
                <button className="btn btn-outline-primary btn-xs" type="button" onClick={_handleClickEditRow}>
                    <i className="fas fa-paperclip"></i>
                </button>&nbsp;
                <button className="btn btn-outline-info btn-xs" type="button" onClick={_handleClickCopyRow}>
                    <i className="fas fa-copy"></i>
                </button>&nbsp;
                <button className="btn btn-outline-danger btn-xs" type="button" onClick={_handleClickDelRow}>
                    <i className="fas fa-trash"></i>
                </button>
                <div className="modal fade" tabIndex="-1" role="dialog" id={"accDetailLine"+props.index}>
                    <div className="modal-dialog modal-md" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Detalle</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">                                       
                            <form>
                                <div className="form-group row">
                                    <label htmlFor={"inputDetailRut"+props.index}
                                        className="col-md-3 col-lg-3 col-form-label col-form-label-xs">
                                            Rut
                                    </label>
                                    <div className="col-md-3 col-lg-3">
                                        <input className="form-control form-control-xs" 
                                            id={"inputDetailRut"+props.index}
                                            type="text" 
                                            placeholder=""
                                            value={props.rut}
                                            onChange={_handleChangeRut}
                                            onBlur={_handleValidarRut} 
                                        />
                                    </div>                            
                                </div> 
                                <div className="form-group row">
                                    <label htmlFor={"inputDetailRut"+props.index}
                                        className="col-md-12 col-lg-12 col-form-label col-form-label-xs">
                                            {props.nombre}
                                    </label>
                                </div> 
                                <div className="form-group row">
                                    <label htmlFor={"inputDetailDetalle"+props.index}
                                        className="col-md-3 col-lg-3 col-form-label col-form-label-xs">
                                        Descripci&oacute;n
                                    </label>
                                    <div className="col-md-9 col-lg-9">
                                        <textarea className="form-control form-control-xs" 
                                            id={"inputDetailDetalle"+props.index}
                                            type="text" 
                                            placeholder=""
                                            value={props.detalle}
                                            onChange={_handleChangeDetalle} 
                                        >
                                        </textarea>
                                    </div>                        
                                </div> 
                                <div className="form-check row">
                                    <input className="form-check-input" 
                                        id={"inputDetailTodas"+props.index}
                                        type="checkbox" 
                                        checked={props.todas}
                                        onChange={_handleChangeTodas} 
                                    />
                                    <label className="form-check-label" 
                                        htmlFor={"inputDetailTodas"+props.index}
                                    >
                                        Repetir en todas las l&iacute;neas nuevas
                                    </label>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={_handleAcceptAccDetailLine}>Aceptar</button>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={_handleCloseAccDetailLine}>Cerrar</button>
                        </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    )
}