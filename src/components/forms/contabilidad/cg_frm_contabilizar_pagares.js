import { config } from '../../_constants';
import {base64EncodeUnicode} from '../../_helpers';
import React, {Component} from 'react';
import $ from 'jquery';

import {MenuForm} from '../menuform/menuform'

export class CgFrmContabilizarPagares extends Component{

    constructor(props){ 
        super(props);

        this.user = this.props.authentication.user;
        this.company_id = this.user.company_used;
        this.hoy = new Date();
        this.mes = this.hoy.getMonth();
        this.year = this.hoy.getFullYear();
        this.fecha = this.year+'-'+('0' + (this.mes + 1)).slice(-2)+'-'+('0' + this.hoy.getDate()).slice(-2); 

        this.meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];        
        this.style_radio = {
            marginLeft: '0px',
            marginRight: '5px'
        };

        this.state = {
            general: {id: "", company_id: this.company_id, entry_year: this.year, entry_month: (this.mes+1), entry_date: this.fecha,create_module: "CONTABILIDAD"},
            filtros: {selectedOption: "FECHA", date_start: this.fecha, date_end: this.fecha, period_txt: this.meses[this.mes]+" "+this.year, period_month: (this.mes+1), period_year: this.year, selectAll: false},
            detalle: [],
            menuitemss: []
        }

        this._handleClickCambiarPeriodo = this._handleClickCambiarPeriodo.bind(this);
        this._handleClickPeriodoAtras = this._handleClickPeriodoAtras.bind(this);
        this._handleClickPeriodoAdelante = this._handleClickPeriodoAdelante.bind(this);

        this._handleChangeOption = this._handleChangeOption.bind(this);
        this._handleChangeDateStart = this._handleChangeDateStart.bind(this);
        this._handleChangeDateEnd = this._handleChangeDateEnd.bind(this);
        this._handleClickBuscar = this._handleClickBuscar.bind(this);
        this._handleChangeContabilizar = this._handleChangeContabilizar.bind(this);
        this._handleChangeCheckboxSelectAll = this._handleChangeCheckboxSelectAll.bind(this);
        this._handleOnClickBtnLimpiar = this._handleOnClickBtnLimpiar.bind(this);
        this._handleOnClickBtnGuardar = this._handleOnClickBtnGuardar.bind(this);
        this._handleClickVerComprobante = this._handleClickVerComprobante.bind(this);
    }

    _handleClickVerComprobante(p_entry_id){
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("106");
        let v_entry_id = p_entry_id;

        if(v_entry_id===""){
            return false;
        }

        window.open(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}&var1=${v_entry_id}`);
    }

    _handleChangeOption(e){
        let v_filtros = this.state.filtros;
        v_filtros.selectedOption = e.target.value;
        this.setState({filtros: v_filtros});

    }

    _handleChangeDateStart(e){
        let v_filtros = this.state.filtros;
        v_filtros.date_start = e.target.value;
        this.setState({filtros: v_filtros});
    }

    _handleChangeDateEnd(e){
        let v_filtros = this.state.filtros;
        v_filtros.date_end = e.target.value;
        this.setState({filtros: v_filtros});
    }

    _handleClickBuscar(e){
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("107");

        const formData = new FormData();
        formData.append( "var1", this.state.filtros.selectedOption);
        formData.append( "var2", this.state.filtros.date_start);
        formData.append( "var3", this.state.filtros.date_end);
        formData.append( "var4", this.state.filtros.period_month);
        formData.append( "var5", this.state.filtros.period_year);
        
        const requestOptions = { 
            method: 'POST',
            body: formData
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const v_response = await response.json();
                let v_data = v_response.data;
                if(v_data.existe===true){               
                    let v_pagares = v_data.pagares;

                    let v_detalle = v_pagares.map((row, index) => {
                        let v_row = {
                                        folio: row.ipg_cod, fecha: row.ipg_fecha, 
                                        deudor: row.ipg_deudor_nombre, pacientes: row.pacientes,
                                        total: row.ipg_total,contabilizar: row.contabilizar,entry_id: row.entry_id, entry_code: row.entry_code, 
                                        contabilizado: row.contabilizado, cuotas: row.ipg_ncuota, detalle_cuotas: row.detalle_cuotas,
                                        presupuestos: row.presupuestos
                                    };
                        
                        return v_row;
                        
                    });

                    this.setState({detalle: v_detalle});
                    
                    this.setStateMenuItem("GUARDAR");
                }
                else{
                    this.setStateMenuItem("DEFAULT");
                }
            })
			.catch((error) => {
				console.error('Error:', error);
			});
    }

    _handleChangeCheckboxSelectAll(e){
        var v_filtros = this.state.filtros;        
        if(v_filtros.selectAll){
            v_filtros.selectAll = false;
        }
        else{
            v_filtros.selectAll = true;
        }
        this.setState({filtros: v_filtros});

        let v_detalle = this.state.detalle.map(function(row,index){
            row.contabilizar = v_filtros.selectAll;
            return row;
        });
        this.setState({detalle: v_detalle});
    }

    _handleChangeContabilizar(p_index){
         
        let v_detalle = this.state.detalle.map(function(row,index){
            if(index===p_index){
                if(row.contabilizar){
                    row.contabilizar = false;
                }
                else{
                    row.contabilizar = true;
                }
            }
            return row;
        });
        
        this.setState({detalle: v_detalle});
        var v_filtros = this.state.filtros;  
        var v_existenFalsos = false;
        this.state.detalle.forEach(function(row,index){
            if(!row.contabilizar){
                v_existenFalsos = true;
            }
        });

        if(v_filtros.selectAll && v_existenFalsos){
            v_filtros.selectAll = false;
        }
        this.setState({filtros: v_filtros});
    }

    _handleOnClickBtnGuardar(e){        
        let user = JSON.parse(localStorage.getItem('user'));
        let v_company = base64EncodeUnicode(user.company_used);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("108");
        var v_datos = {general: this.state.general, detalle: this.state.detalle};

        const requestOptions = { 
            method: 'POST',
            //headers: authHeader(),
            body: JSON.stringify(v_datos)
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const v_response = await response.json();                
                
                let v_data = v_response.data;
                let v_errores = v_response.errores;
                let v_status = v_response.status;
                //let v_message = v_response.message;
                var v_error_message = "";
                
                if(v_status==="error"){
                    v_errores.forEach((error,index) => {
                        if(v_error_message.length===0){
                            v_error_message = error;
                        }
                        else{
                            v_error_message += "\n"+error;
                        }
                    });
                    alert("Ocurrieron los siguientes errores al guardar los registros:\n"+v_error_message);
                }
                else{                    
                    let v_general = this.state.general;
                    v_general.id = v_data.general.id;
                    this.setState({general: v_general});

                    let v_detalle = this.state.detalle;
                    v_detalle = v_data.detalle.map((row, index) => {
                        let v_row = {
                                        folio: row.folio, fecha: row.fecha, 
                                        deudor: row.deudor, pacientes: row.pacientes,
                                        total: row.total,contabilizar: row.contabilizar, 
                                        entry_id: row.entry_id, entry_code: row.entry_code, 
                                        contabilizado: row.contabilizado, cuotas: row.cuotas, detalle_cuotas: row.detalle_cuotas,
                                        presupuestos: row.presupuestos
                                    };
                        
                        return v_row;
                        
                    });

                    this.setState({detalle: v_detalle});
                }
            })
			.catch((error) => {
				console.error('Error:', error);
			});
    }

    _handleOnClickBtnLimpiar(e){

        this.setState({
            general: {id: "", company_id: this.company_id, entry_year: this.year, entry_month: (this.mes+1), entry_date: this.fecha,create_module: "CONTABLE"},
            filtros: {selectedOption: "FECHA", date_start: this.fecha, date_end: this.fecha, period_txt: this.meses[this.mes]+" "+this.year, period_month: (this.mes+1), period_year: this.year, selectAll: false},
            detalle: [],
            menuitemss: []
        });

        this.setStateMenuItem("DEFAULT");

    }

    _handleClickCambiarPeriodo(event){
        console.log("event",event.target.dataset);
        let opcion = event.target.dataset.opcion;
        let v_filtros = this.state.filtros;
        let v_mes = v_filtros.period_month;
        let v_anho = v_filtros.period_year;
        
        if(opcion==='atras'){
            v_mes = v_mes - 1;
            if(v_mes===0){
              v_anho = v_anho -1;
              v_mes = 12;
            }
        }
        else{
            v_mes = parseInt(v_mes) + 1;
            if(v_mes===13){
              v_anho = parseInt(v_anho) + 1;
              v_mes = 1;
            }
        }

        v_filtros.period_month = v_mes;
        v_filtros.period_year = v_anho;
        v_filtros.period_txt = this.meses[parseInt(v_mes)-1]+" "+v_anho;

        this.setState({filtros: v_filtros});
    } 

    _handleClickPeriodoAtras(event){

        let v_filtros = this.state.filtros;
        let v_mes = v_filtros.period_month;
        let v_anho = v_filtros.period_year;
        v_mes = v_mes - 1;
        if(v_mes===0){
          v_anho = v_anho -1;
          v_mes = 12;
        }
        v_filtros.period_month = v_mes;
        v_filtros.period_year = v_anho;
        v_filtros.period_txt = this.meses[parseInt(v_mes)-1]+" "+v_anho;

        this.setState({filtros: v_filtros});
    }

    _handleClickPeriodoAdelante(event){
        
        let v_filtros = this.state.filtros;
        let v_mes = v_filtros.period_month;
        let v_anho = v_filtros.period_year;
        v_mes = parseInt(v_mes) + 1;
        if(v_mes===13){
            v_anho = parseInt(v_anho) + 1;
            v_mes = 1;
        }

        v_filtros.period_month = v_mes;
        v_filtros.period_year = v_anho;
        v_filtros.period_txt = this.meses[parseInt(v_mes)-1]+" "+v_anho;

        this.setState({filtros: v_filtros});
    }

    setStateMenuItem(p_option){
        let menuitems = [];
        if(p_option==="DEFAULT"){
            menuitems = [ 
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        else if(p_option==="GUARDAR"){
            menuitems = [ 
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: false},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        }
        
        this.setState({menuitemss: menuitems});
    }

    componentWillMount() {
        const menuitems = [ 
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar, disabled: true},
                            {type: 'LIMPIAR', onClick: this._handleOnClickBtnLimpiar, disabled: false}
                        ];
        
        this.setState({menuitemss: menuitems});
    }
    
    render(){  
        console.log("cg_frm_centralizar_pagares");

        return(
        <>
            <MenuForm menuitems={this.state.menuitemss} />
            <div className="row">
                <div className="col-lg-12">
                <div className="card">
                    <div className="card-header">
                        <strong>Par&aacute;metros de B&uacute;squeda</strong>
                    </div>
                    <div className="card-body">                    
                        <form>
                            <div className="form-group row" style={{marginBottom: '10px'}}>
                                <div className="col-md-2 offset-md-2">
                                    <input 
                                        type="radio" 
                                        value="FECHA" 
                                        name="option"
                                        checked={this.state.filtros.selectedOption==="FECHA"}
                                        onChange={this._handleChangeOption} 
                                    />
                                    &nbsp;Por Fecha
                                </div>
                                <div className="col-md-2 col-lg-2">
                                    <input 
                                        type="radio" 
                                        value="PERIODO" 
                                        name="option"
                                        checked={this.state.filtros.selectedOption==="PERIODO"}
                                        onChange={this._handleChangeOption} 
                                    />
                                    &nbsp;Por Periodo
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="fecha_inicio" 
                                    className="col-md-2 col-lg-2 col-form-label col-form-label-xs">
                                        Fecha Inicio
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <input className="form-control form-control-xs" 
                                        id="fecha_inicio" 
                                        type="date" 
                                        placeholder=""
                                        value={this.state.filtros.date_start}
                                        onChange={this._handleChangeDateStart} 
                                        readOnly={this.state.filtros.selectedOption!="FECHA"}
                                    />
                                </div>
                                <label htmlFor="fecha_termino" 
                                    className="col-md-2 col-lg-2 col-form-label col-form-label-xs">
                                        Fecha T&eacute;rmino
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <input className="form-control form-control-xs" 
                                        id="fecha_termino" 
                                        type="date" 
                                        placeholder=""
                                        value={this.state.filtros.date_end}
                                        onChange={this._handleChangeDateEnd} 
                                        readOnly={this.state.filtros.selectedOption!="FECHA"}
                                    />
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="inputTipoComprobante" 
                                    className="col-md-2 col-lg-2 col-form-label col-form-label-xs">
                                    Periodo
                                </label>
                                <div className="col-md-3 col-lg-3">
                                    <div className="input-group input-group-xs">
                                        <div className="input-group-append">
                                            <button className="btn btn-outline-info btn-sm" 
                                                type="button"
                                                data-opcion="atras"
                                                onClick={this._handleClickPeriodoAtras} 
                                                disabled={this.state.filtros.selectedOption!="PERIODO"}
                                            >
                                                <i className="fas fa-arrow-left">&nbsp;</i>
                                            </button>
                                        </div>
                                        <input className="form-control py-1" 
                                            id="inputTipoComprobante" 
                                            type="text" 
                                            placeholder=""
                                            value={this.state.filtros.period_txt}
                                            readOnly={true}
                                            style={{textAlign: "center"}}
                                        />
                                        <div className="input-group-append">
                                            <button className="btn btn-outline-info btn-sm" 
                                                type="button" 
                                                data-opcion="adelante"
                                                onClick={this._handleClickPeriodoAdelante} 
                                                disabled={this.state.filtros.selectedOption!="PERIODO"}
                                            >
                                                <i className="fas fa-arrow-right">&nbsp;</i>
                                            </button>
                                        </div>
                                    </div>   
                                </div> 
                                <div className="col-md-3 offset-md-1 col-lg-3 offset-lg-1">
                                    <button className="btn btn-outline-info btn-xs" type="button" title="Cerrar Formulario" style={this.style} onClick={this._handleClickBuscar}><i className='fa fa-search' ></i>&nbsp;Buscar</button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <br />
            <div className="row">
                <div className="col-lg-12">
                <div className="card">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <strong>Listado de Pagares</strong>&nbsp;
                    </div>
                    <div className="card-body">
                        <table className="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th scope="col" style={{width: '40px', textAlign: 'center'}}>
                                        <input 
                                            type="checkbox" 
                                            checked={this.state.filtros.selectAll}
                                            onChange={this._handleChangeCheckboxSelectAll} 
                                        /></th>
                                    <th scope="col" style={{width: '80px', textAlign: 'center'}}>Folio</th>
                                    <th scope="col" style={{width: '100px', textAlign: 'center'}}>Fecha</th>
                                    <th scope="col" style={{width: '280px', textAlign: 'center'}}>Deudor</th>
                                    <th scope="col" style={{textAlign: 'center'}}>Paciente</th>
                                    <th scope="col" style={{width: '100px', textAlign: 'center'}}>Total Pagar&eacute;</th>
                                    <th scope="col" style={{width: '80px', textAlign: 'center'}}>Comprobante</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.detalle.map((row, index) => {
                                        return(  
                                            <Detalle 
                                                key={index}
                                                index={index}
                                                folio={row.folio} 
                                                fecha={row.fecha}
                                                deudor={row.deudor}
                                                pacientes={row.pacientes}
                                                total={row.total}
                                                contabilizar={row.contabilizar}
                                                contabilizarRow={this._handleChangeContabilizar}
                                                entry_id={row.entry_id}
                                                entry_code={row.entry_code}
                                                contabilizado={row.contabilizado}
                                                verComprobante={this._handleClickVerComprobante}
                                            />
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>        
        </>
      );
    }
}
  
function Detalle(props){

    function _handleChangeCheckbox(e){
        props.contabilizarRow(props.index);
    }

    function _handleClickVer(e){
        props.verComprobante(props.entry_id);
    }

    return(
        <tr>
            <td scope="row" style={{textAlign: 'center'}}>
                <input 
                    type="checkbox" 
                    checked={props.contabilizar}
                    onChange={_handleChangeCheckbox}
                    disabled={props.contabilizado}
                />
            </td>
            <td scope="row" align="center">{props.folio}</td>
            <td scope="row" align="center">{props.fecha}</td>
            <td scope="row">{props.deudor}</td>
            <td scope="row">{props.pacientes}</td>
            <td scope="row" align='right'>{props.total}</td>
            <td scope="row" align='center'><a href="/#" onClick={_handleClickVer}>{(props.entry_code!="")? props.entry_code+" (Ver)": ""}</a></td>
        </tr>
    )
}