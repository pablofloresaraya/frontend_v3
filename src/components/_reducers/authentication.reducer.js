import { userConstants } from '../_constants';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
      //return ;
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:      
      return {};
    case userConstants.COMPANY_USED:
      let v_state = state;
      v_state.user.company_used = action.company_id;
      return {
        user: v_state.user
      };
      
    default:
      return state
  }
}