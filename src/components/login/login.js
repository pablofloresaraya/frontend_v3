import React, {Component} from 'react';
//import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class Login extends Component{
    constructor(props) {
        super(props);

        // reset login status
        this.props.logout();

        this.state = {
            username: 'admin', 
            password: '12345',
            submitted: false
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        
        this.styles_layoutSidenav_content = {
          top: '41px',
        }
    }

    _handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    _handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        if (username && password) {
            this.props.login(username, password);
        }
    }
  
    render(){
        const { loggedIn } = this.props;
        const { username, password, submitted } = this.state;

      return (
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-5">
                                <form name="form" onSubmit={this._handleSubmit}>
                                    <div className="card shadow-lg border-0 rounded-lg mt-5">
                                        <div className="card-header"><h3 className="text-center font-weight-light my-4">Ingresar</h3></div>
                                        <div className="card-body">                                            
                                            <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                                                <label className="small mb-1" htmlFor="inputEmailAddress">Usuario</label>
                                                <input 
                                                    className="form-control py-4" 
                                                    name="username"
                                                    type="text" 
                                                    placeholder="Usuario"
                                                    value={username} 
                                                    onChange={this._handleChange} />
                                                {submitted && !username &&
                                                    <div className="help-block">Usuario requerido</div>
                                                }
                                            </div>
                                            <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                                                <label className="small mb-1" htmlFor="inputPassword">Contrase&ntilde;a</label>
                                                <input 
                                                    className="form-control py-4" 
                                                    name="password"
                                                    type="password" 
                                                    placeholder="Contraseña"
                                                    value={password} 
                                                    onChange={this._handleChange}  
                                                />
                                                {submitted && !password &&
                                                    <div className="help-block">Contrase&ntilde;a requerida</div>
                                                }
                                            </div>
                                            <div 
                                                className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                <a className="small" href="password.html">Olvid&oacute; su contrase&ntilde;a?</a>
                                                <button className="btn btn-primary">Ingresar</button>
                                                {loggedIn  &&
                                                    <img 
                                                        src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" 
                                                        alt=""
                                                    />
                                                }
                                            </div>
                                            
                                        </div>
                                        <div className="card-footer text-center">
                                            <div className="small">
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer className="py-4 bg-light mt-auto">
                    <div className="container-fluid">
                        <div className="d-flex align-items-center justify-content-between small">
                            <div className="text-muted">Copyright &copy; Klinik</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
      );
    }
  }

  function mapState(state) {
      const { loggedIn } = state.authentication;
      return { loggedIn };
  }
  
  const actionCreators = {
      login: userActions.login,
      logout: userActions.logout
  };
  
  const connectedLoginPage = connect(mapState, actionCreators)(Login);
  export { connectedLoginPage as Login };